package me.voidaspect.zone15.availability.exception;

import io.grpc.Status;

public class ResourceNotFoundException extends AvailabilityServiceException {

    public ResourceNotFoundException(long id) {
        super(Status.Code.NOT_FOUND, "Resource with id " + id + " was not found");
    }
}
