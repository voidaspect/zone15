package me.voidaspect.zone15.availability.exception;

import io.grpc.Status;

public class ResourceUnavailableException extends AvailabilityServiceException {

    public ResourceUnavailableException(long id) {
        super(Status.Code.FAILED_PRECONDITION, "Resource with id " + id + " is not available");
    }
}
