package me.voidaspect.zone15.availability.service;

import me.voidaspect.zone15.availability.exception.DataConstraintException;
import me.voidaspect.zone15.availability.exception.ResourceNotFoundException;
import me.voidaspect.zone15.availability.exception.ResourceUnavailableException;
import me.voidaspect.zone15.availability.model.Resource;
import me.voidaspect.zone15.availability.repository.ResourceRepository;
import me.voidaspect.zone15.availability.rpc.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ResourceService implements ResourceOperations {

    private final ResourceRepository resourceRepository;

    public ResourceService(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    @Override
    @Transactional
    public long create(CreateResourceRequest request) throws DataConstraintException {
        var name = request.getName();
        if (name == null || name.isBlank()) {
            throw DataConstraintException.emptyResourceName();
        }
        if (resourceRepository.existsByName(name)) {
            throw DataConstraintException.duplicateResourceName(name);
        }
        int capacity = request.getCapacity();
        if (capacity < 1) {
            throw DataConstraintException.nonPositiveCapacity();
        }

        var resource = new Resource();
        resource.setName(name);
        resource.setDescription(request.getDescription());
        resource.setCapacity(capacity);

        resourceRepository.save(resource);

        return resource.getId();
    }

    @Override
    @Transactional
    public void delete(long id) throws ResourceNotFoundException, ResourceUnavailableException {
        var resource = getResource(id);
        if (resource.getParties() > 0) {
            throw new ResourceUnavailableException(id);
        }
        resourceRepository.delete(resource);
    }

    @Override
    @Transactional(readOnly = true)
    public ResourceResponse findById(long id) throws ResourceNotFoundException {
        var resource = getResource(id);

        return ResourceResponse.newBuilder()
                .setName(resource.getName())
                .setDescription(resource.getDescription())
                .setCapacity(resource.getCapacity())
                .setParties(resource.getParties())
                .build();
    }

    @Override
    @Transactional(readOnly = true)
    public ResourceAvailabilityResponse checkAvailability(long id, int parties) throws ResourceNotFoundException {
        var resource = getResource(id);
        int presentParties = resource.getParties();
        int capacity = resource.getCapacity();
        boolean available = presentParties + parties <= capacity;

        return ResourceAvailabilityResponse.newBuilder()
                .setAvailable(available)
                .setCapacity(capacity)
                .setParties(presentParties)
                .build();
    }

    @Override
    @Transactional
    public ResourceUsageResponse acquire(long id, int parties) throws ResourceNotFoundException, ResourceUnavailableException {
        var resource = getResource(id);
        int totalParties = resource.getParties() + parties;
        int capacity = resource.getCapacity();
        if (totalParties > capacity) {
            throw new ResourceUnavailableException(id);
        }
        resource.setParties(totalParties);

        return ResourceUsageResponse.newBuilder()
                .setCapacity(capacity)
                .setParties(totalParties)
                .build();
    }

    @Override
    @Transactional
    public ResourceUsageResponse release(long id, int parties) throws ResourceNotFoundException {
        var resource = getResource(id);
        int totalParties = Math.max(0, resource.getParties() - parties);
        resource.setParties(totalParties);

        return ResourceUsageResponse.newBuilder()
                .setParties(totalParties)
                .setCapacity(resource.getCapacity())
                .build();
    }

    private Resource getResource(long id) throws ResourceNotFoundException {
        return resourceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));
    }

}
