package me.voidaspect.zone15.availability.repository;

import me.voidaspect.zone15.availability.model.Resource;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResourceRepository extends JpaRepository<Resource, Long> {

    boolean existsByName(String name);

}
