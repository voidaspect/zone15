package me.voidaspect.zone15.availability.exception;

import io.grpc.Status;
import io.grpc.StatusException;

public class AvailabilityServiceException extends Exception {

    private final Status.Code grpcStatusCode;

    public AvailabilityServiceException(Status.Code grpcStatusCode, String message) {
        super(message);
        this.grpcStatusCode = grpcStatusCode;
    }

    public AvailabilityServiceException(Status.Code grpcStatusCode, String message, Throwable cause) {
        super(message, cause);
        this.grpcStatusCode = grpcStatusCode;
    }

    public Status.Code getGrpcStatusCode() {
        return grpcStatusCode;
    }

    public StatusException toStatusException() {
        return new StatusException(grpcStatusCode.toStatus().withDescription(getLocalizedMessage()));
    }

}
