package me.voidaspect.zone15.availability.service;

import me.voidaspect.zone15.availability.exception.DataConstraintException;
import me.voidaspect.zone15.availability.exception.ResourceNotFoundException;
import me.voidaspect.zone15.availability.exception.ResourceUnavailableException;
import me.voidaspect.zone15.availability.rpc.*;

public interface ResourceOperations {

    long create(CreateResourceRequest request) throws DataConstraintException;

    void delete(long id) throws ResourceNotFoundException, ResourceUnavailableException;

    ResourceResponse findById(long id) throws ResourceNotFoundException;

    ResourceAvailabilityResponse checkAvailability(long id, int parties) throws ResourceNotFoundException;

    ResourceUsageResponse acquire(long id, int parties) throws ResourceNotFoundException, ResourceUnavailableException;

    ResourceUsageResponse release(long id, int parties) throws ResourceNotFoundException;

}
