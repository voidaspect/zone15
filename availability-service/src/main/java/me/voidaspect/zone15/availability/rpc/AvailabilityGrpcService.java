package me.voidaspect.zone15.availability.rpc;

import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import me.voidaspect.zone15.availability.exception.AvailabilityServiceException;
import me.voidaspect.zone15.availability.service.ResourceOperations;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class AvailabilityGrpcService extends AvailabilityGrpc.AvailabilityImplBase {

    private final ResourceOperations resources;

    public AvailabilityGrpcService(ResourceOperations resources) {
        this.resources = resources;
    }

    @Override
    public void createResource(CreateResourceRequest request, StreamObserver<ResourceId> responseObserver) {
        try {
            long id = resources.create(request);
            responseObserver.onNext(ResourceId.newBuilder().setId(id).build());
            responseObserver.onCompleted();
        } catch (AvailabilityServiceException e) {
            responseObserver.onError(e.toStatusException());
        }
    }

    @Override
    public void deleteResource(ResourceId request, StreamObserver<Empty> responseObserver) {
        try {
            resources.delete(request.getId());
            responseObserver.onNext(Empty.getDefaultInstance());
            responseObserver.onCompleted();
        } catch (AvailabilityServiceException e) {
            responseObserver.onError(e.toStatusException());
        }
    }

    @Override
    public void getResource(ResourceId request, StreamObserver<ResourceResponse> responseObserver) {
        try {
            var response = resources.findById(request.getId());
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        } catch (AvailabilityServiceException e) {
            responseObserver.onError(e.toStatusException());
        }
    }

    @Override
    public void checkAvailability(CheckResourceAvailabilityRequest request, StreamObserver<ResourceAvailabilityResponse> responseObserver) {
        try {
            var response = resources.checkAvailability(request.getId(), request.getParties());
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        } catch (AvailabilityServiceException e) {
            responseObserver.onError(e.toStatusException());
        }
    }

    @Override
    public void acquireResource(AcquireResourceRequest request, StreamObserver<ResourceUsageResponse> responseObserver) {
        try {
            var response = resources.acquire(request.getId(), request.getParties());
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        } catch (AvailabilityServiceException e) {
            responseObserver.onError(e.toStatusException());
        }
    }

    @Override
    public void releaseResource(ReleaseResourceRequest request, StreamObserver<ResourceUsageResponse> responseObserver) {
        try {
            var response = resources.release(request.getId(), request.getParties());
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        } catch (AvailabilityServiceException e) {
            responseObserver.onError(e.toStatusException());
        }
    }
}
