package me.voidaspect.zone15.availability.exception;

import io.grpc.Status;

public class DataConstraintException extends AvailabilityServiceException {

    public DataConstraintException(String message) {
        super(Status.Code.FAILED_PRECONDITION, message);
    }

    public static DataConstraintException duplicateResourceName(String name) {
        return new DataConstraintException("Resource with name \"" + name + "\" already exists");
    }

    public static DataConstraintException emptyResourceName() {
        return new DataConstraintException("Resource should have a non-empty name");
    }

    public static DataConstraintException nonPositiveCapacity() {
        return new DataConstraintException("Resource capacity should be greater than 0");
    }

}
