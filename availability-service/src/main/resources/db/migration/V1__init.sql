create sequence resources_id_seq increment 10;

create table resources
(
    id          bigint  not null default nextval('resources_id_seq'),
    name        text    not null,
    description text,
    capacity    integer not null,
    parties     integer not null default 0,

    primary key (id),
    unique (name),
    check ( capacity > 1 ),
    check ( parties >= 0 and parties <= capacity )
);