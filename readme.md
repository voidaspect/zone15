# Zone15

Demo for Spring Boot 2.4, Java 15 and gRPC

## Services

Here is a list of Zone15 services

### Ticket Service

Responsible for the issuance and usage of tickets
Owns personal data of ticket holders.

### Availability service

Allows other services to acquire and release shared resources.