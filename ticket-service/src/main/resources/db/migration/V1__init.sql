-- mapping of 'gender' enum
create table genders
(
    id    smallint not null primary key,
    value text     not null
);

insert into genders
values (0, 'MALE'),
       (1, 'FEMALE'),
       (2, 'UNSPECIFIED');

create sequence holders_id_seq increment 10;

create table holders
(
    id            bigint   not null default nextval('holders_id_seq'),
    phone_number  text     not null,
    email         text,
    first_name    text     not null,
    middle_name   text,
    last_name     text     not null,
    date_of_birth date     not null,
    gender        smallint not null default 2,
    version       integer  not null default 0,

    primary key (id),
    unique (phone_number),
    unique (email),
    foreign key (gender) references genders (id),

    check ( date_of_birth < current_date )
);

-- mapping of 'ticket status' enum
create table ticket_statuses
(
    id          smallint not null primary key,
    value       text     not null,
    description text     not null
);

insert into ticket_statuses
values (0, 'REQUESTED', 'An end-user requested a ticket'),
       (1, 'CONFIRMED', 'Zone15 confirmed end-user''s request for a ticket'),
       (2, 'REJECTED', 'Zone15 rejected end-user''s request for a ticket'),
       (3, 'CANCELLED', 'An end-user cancelled this ticket'),
       (4, 'REVOKED', 'Zone15 revoked this ticket'),
       (5, 'USED', 'This ticket was fully used');

create sequence tickets_id_seq increment 10;

create table tickets
(
    id           bigint    not null default nextval('tickets_id_seq'),
    holder_id    bigint    not null,
    status       smallint  not null,
    max_usages   smallint  not null default 1,
    created_at   timestamp not null default current_timestamp,
    activates_at timestamp not null default current_timestamp,
    expires_at   timestamp,
    version      integer   not null default 0,

    primary key (id),
    foreign key (holder_id) references holders (id) on delete cascade,
    foreign key (status) references ticket_statuses (id),

    check ( max_usages > 0 ),
    check ( expires_at is null or (expires_at > activates_at and expires_at > created_at) )
);

create index on tickets (holder_id);

create table ticket_usage_history
(
    ticket_id    bigint    not null,
    usage_number smallint  not null,
    used_at      timestamp not null,

    primary key (ticket_id, usage_number),
    foreign key (ticket_id) references tickets (id) on delete cascade,

    check ( used_at <= current_timestamp )
);

create table ticket_status_history
(
    ticket_id     bigint    not null,
    changed_at    timestamp not null default current_timestamp,
    status_before smallint  not null,
    status_after  smallint  not null,

    primary key (ticket_id, status_after),
    unique (ticket_id, status_before),
    foreign key (ticket_id) references tickets (id) on delete cascade,
    foreign key (status_before) references ticket_statuses (id),
    foreign key (status_after) references ticket_statuses (id),

    check ( changed_at <= current_timestamp )
);