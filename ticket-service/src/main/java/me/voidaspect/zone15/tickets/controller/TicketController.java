package me.voidaspect.zone15.tickets.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import me.voidaspect.zone15.tickets.Routes;
import me.voidaspect.zone15.tickets.config.springdoc.ConstraintViolationApiResponse;
import me.voidaspect.zone15.tickets.config.springdoc.InvalidInputApiResponse;
import me.voidaspect.zone15.tickets.config.springdoc.TicketNotFoundApiResponse;
import me.voidaspect.zone15.tickets.exception.ticket.InvalidStatusChangeException;
import me.voidaspect.zone15.tickets.exception.ticket.InvalidUsageException;
import me.voidaspect.zone15.tickets.exception.ticket.TicketNotFoundException;
import me.voidaspect.zone15.tickets.model.ticket.request.AddTicketUsageRequest;
import me.voidaspect.zone15.tickets.model.ticket.request.MergeTicketRequest;
import me.voidaspect.zone15.tickets.model.ticket.response.DetailedTicketResponse;
import me.voidaspect.zone15.tickets.service.ticket.TicketOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(Routes.TICKETS)
public class TicketController {

    private final TicketOperations tickets;

    public TicketController(TicketOperations tickets) {
        this.tickets = tickets;
    }

    @Operation(summary = "Find Ticket by id")
    @ApiResponse(responseCode = "200", description = "Ticket found",
            content = @Content(schema = @Schema(implementation = DetailedTicketResponse.class)))
    @TicketNotFoundApiResponse
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DetailedTicketResponse findById(@PathVariable long id) throws TicketNotFoundException {

        return tickets.findById(id).orElseThrow(() -> new TicketNotFoundException(id));
    }

    @Operation(summary = "Merge Ticket by id")
    @ApiResponse(responseCode = "200", description = "Ticket merged",
            content = @Content(schema = @Schema(implementation = DetailedTicketResponse.class)))
    @InvalidInputApiResponse
    @TicketNotFoundApiResponse
    @ConstraintViolationApiResponse
    @PatchMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DetailedTicketResponse merge(
            @PathVariable long id,
            @RequestBody @Valid MergeTicketRequest request,
            @RequestParam(name = "drop-optional-fields", defaultValue = "false") boolean dropOptionalFields
    ) throws TicketNotFoundException, InvalidStatusChangeException {

        return tickets.merge(id, request, dropOptionalFields);
    }

    @Operation(summary = "Add Usage record to a Ticket by id")
    @ApiResponse(responseCode = "200", description = "Ticket Usage added",
            content = @Content(schema = @Schema(implementation = DetailedTicketResponse.class)))
    @InvalidInputApiResponse
    @TicketNotFoundApiResponse
    @ConstraintViolationApiResponse
    @PostMapping(
            value = "/{id}/usages",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.CREATED)
    public DetailedTicketResponse addUsage(
            @PathVariable long id,
            @RequestBody @Valid AddTicketUsageRequest request
    ) throws InvalidUsageException, TicketNotFoundException {

        return tickets.addUsage(id, request);
    }

}
