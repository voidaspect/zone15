package me.voidaspect.zone15.tickets.config.springdoc;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import me.voidaspect.zone15.tickets.model.error.BindingErrorErrorResponse;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
@ApiResponse(responseCode = "400", description = "Invalid input",
        content = @Content(schema = @Schema(implementation = BindingErrorErrorResponse.class)))
public @interface InvalidInputApiResponse {
}
