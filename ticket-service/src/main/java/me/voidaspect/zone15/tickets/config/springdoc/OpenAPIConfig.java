package me.voidaspect.zone15.tickets.config.springdoc;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class OpenAPIConfig {

    private static final String TITLE = "Zone15 Ticket Service";

    private static final String DESCRIPTION = "API for managing Zone15 Tickets and Ticket Holders";

    @Bean
    public OpenAPI openAPI(BuildProperties buildProperties) {
        var info = new Info()
                .title(TITLE)
                .description(DESCRIPTION)
                .version(buildProperties.getVersion());
        return new OpenAPI()
                .info(info);
    }

}
