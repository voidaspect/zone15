package me.voidaspect.zone15.tickets.model.ticket;

import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Enumerated;
import java.io.Serializable;

@Embeddable
public class TicketStatusChangeId implements Serializable {

    @Column(nullable = false, updatable = false)
    private long ticketId;

    @Column(nullable = false, updatable = false, columnDefinition = "int2")
    @Enumerated
    private TicketStatus statusAfter;

    public TicketStatusChangeId() {
    }

    public TicketStatusChangeId(long ticketId, @NotNull TicketStatus statusAfter) {
        this.ticketId = ticketId;
        this.statusAfter = statusAfter;
    }

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public TicketStatus getStatusAfter() {
        return statusAfter;
    }

    public void setStatusAfter(@NotNull TicketStatus statusAfter) {
        this.statusAfter = statusAfter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TicketStatusChangeId id = (TicketStatusChangeId) o;

        return ticketId == id.ticketId && statusAfter == id.statusAfter;
    }

    @Override
    public int hashCode() {
        int result = Long.hashCode(ticketId);
        result = 31 * result + statusAfter.hashCode();
        return result;
    }

}
