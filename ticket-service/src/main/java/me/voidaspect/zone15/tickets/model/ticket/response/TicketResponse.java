package me.voidaspect.zone15.tickets.model.ticket.response;

import com.fasterxml.jackson.annotation.*;
import me.voidaspect.zone15.tickets.Routes;
import me.voidaspect.zone15.tickets.model.ResourceResponse;
import me.voidaspect.zone15.tickets.model.ticket.Ticket;
import me.voidaspect.zone15.tickets.model.ticket.TicketStatus;

import java.time.Instant;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public record TicketResponse(
        long id,
        TicketStatus status,
        short maxUsages,
        Instant createdAt,
        Instant activatesAt,
        @JsonInclude(JsonInclude.Include.NON_NULL) Instant expiresAt,
        @JsonIgnore long holderId
) implements ResourceResponse {

    public static TicketResponse from(Ticket source) {
        return new TicketResponse(
                source.getId(),
                source.getStatus(),
                source.getMaxUsages(),
                source.getCreatedAt(),
                source.getActivatesAt(),
                source.getExpiresAt(),
                source.getHolder().getId()
        );
    }

    @JsonCreator
    public TicketResponse(@JsonProperty(value = "id", required = true) long id,
                          @JsonProperty(value = "status", required = true) TicketStatus status,
                          @JsonProperty(value = "maxUsages", required = true) short maxUsages,
                          @JsonProperty(value = "createdAt", required = true) Instant createdAt,
                          @JsonProperty(value = "activatesAt", required = true) Instant activatesAt,
                          @JsonProperty("expiresAt") Instant expiresAt,
                          @JsonProperty(value = "holder", required = true) TicketHolderReference holder) {

        this(id, status, maxUsages, createdAt, activatesAt, expiresAt, holder.id());
    }

    @Override
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String path() {
        return Routes.ticket(id);
    }

    @JsonProperty
    public TicketHolderReference holder() {
        return new TicketHolderReference(holderId);
    }

}
