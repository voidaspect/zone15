package me.voidaspect.zone15.tickets.model.validation;

import me.voidaspect.zone15.tickets.model.Window;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;

public abstract class WindowValidator<A extends Annotation, T extends Comparable<? super T>, W extends Window<T>> implements ConstraintValidator<A, W> {

    @Override
    public boolean isValid(W value, ConstraintValidatorContext context) {
        return value.start()
                .flatMap(start -> value.finish().map(finish -> finish.compareTo(start) >= 0))
                .orElse(true);
    }

}
