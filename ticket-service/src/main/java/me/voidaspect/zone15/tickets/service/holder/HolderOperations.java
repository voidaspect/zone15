package me.voidaspect.zone15.tickets.service.holder;

import me.voidaspect.zone15.tickets.exception.holder.DuplicateHolderEmailException;
import me.voidaspect.zone15.tickets.exception.holder.DuplicateHolderPhoneNumberException;
import me.voidaspect.zone15.tickets.exception.holder.HolderNotFoundException;
import me.voidaspect.zone15.tickets.model.holder.request.CreateHolderRequest;
import me.voidaspect.zone15.tickets.model.holder.request.MergeHolderRequest;
import me.voidaspect.zone15.tickets.model.holder.response.HolderResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface HolderOperations {

    HolderResponse create(CreateHolderRequest request)
            throws DuplicateHolderEmailException, DuplicateHolderPhoneNumberException;

    Optional<HolderResponse> findById(long id);

    Page<HolderResponse> list(Pageable pageable);

    HolderResponse merge(long id, MergeHolderRequest request, boolean dropOptionalFields)
            throws HolderNotFoundException, DuplicateHolderEmailException, DuplicateHolderPhoneNumberException;

    void delete(long id) throws HolderNotFoundException;

}
