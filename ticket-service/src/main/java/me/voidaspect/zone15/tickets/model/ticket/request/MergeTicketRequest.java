package me.voidaspect.zone15.tickets.model.ticket.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.voidaspect.zone15.tickets.model.Window;
import me.voidaspect.zone15.tickets.model.ticket.TicketStatus;
import me.voidaspect.zone15.tickets.model.validation.TimeWindow;

import javax.validation.constraints.Future;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Optional;

@TimeWindow(message = "Ticket must not expire before activation")
public record MergeTicketRequest(
        @JsonProperty("status")
        TicketStatus status,

        @JsonProperty("activatesAt")
        ZonedDateTime activatesAt,

        @JsonProperty("expiresAt")
        @Future
        ZonedDateTime expiresAt
) implements Window<Instant> {

        @Override
        public Optional<Instant> start() {
                return Optional.ofNullable(activatesAt).map(ZonedDateTime::toInstant);
        }

        @Override
        public Optional<Instant> finish() {
                return Optional.ofNullable(expiresAt).map(ZonedDateTime::toInstant);
        }

}