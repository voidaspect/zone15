package me.voidaspect.zone15.tickets.model.ticket.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.voidaspect.zone15.tickets.model.ticket.TicketUsage;

import java.time.Instant;

public record TicketUsageResponse(
        @JsonProperty("usageNumber") short usageNumber,
        @JsonProperty("usedAt") Instant usedAt
) {

    public static TicketUsageResponse from(TicketUsage source) {
        return new TicketUsageResponse(
                source.getId().getUsageNumber(),
                source.getUsedAt()
        );
    }

}
