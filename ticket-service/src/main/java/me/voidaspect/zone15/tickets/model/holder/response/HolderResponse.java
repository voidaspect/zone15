package me.voidaspect.zone15.tickets.model.holder.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import me.voidaspect.zone15.tickets.Routes;
import me.voidaspect.zone15.tickets.model.ResourceResponse;
import me.voidaspect.zone15.tickets.model.holder.Gender;
import me.voidaspect.zone15.tickets.model.holder.Holder;

import java.time.LocalDate;

@JsonInclude(JsonInclude.Include.NON_NULL)
public record HolderResponse(
        @JsonProperty("id") long id,
        @JsonProperty("phoneNumber") String phoneNumber,
        @JsonProperty("email") String email,
        @JsonProperty("firstName") String firstName,
        @JsonProperty("middleName") String middleName,
        @JsonProperty("lastName") String lastName,
        @JsonProperty("dateOfBirth") LocalDate dateOfBirth,
        @JsonProperty("gender") Gender gender
) implements ResourceResponse {

    public static HolderResponse from(Holder source) {
        return new HolderResponse(
                source.getId(),
                source.getPhoneNumber(),
                source.getEmail(),
                source.getFirstName(),
                source.getMiddleName(),
                source.getLastName(),
                source.getDateOfBirth(),
                source.getGender()
        );
    }

    @Override
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String path() {
        return Routes.holder(id);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public HolderTicketsReference tickets() {
        return new HolderTicketsReference(id);
    }

}
