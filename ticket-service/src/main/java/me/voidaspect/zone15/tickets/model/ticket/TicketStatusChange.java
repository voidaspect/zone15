package me.voidaspect.zone15.tickets.model.ticket;

import org.hibernate.annotations.Immutable;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

@Entity
@Table(name = "ticket_status_history")
@Immutable
public class TicketStatusChange {

    @EmbeddedId
    private TicketStatusChangeId id;

    @MapsId("ticketId")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Ticket ticket;

    @Column(name = "changed_at", nullable = false)
    private Instant changedAt;

    @Column(name = "status_before", nullable = false, columnDefinition = "int2")
    @Enumerated
    private TicketStatus statusBefore;

    //region boilerplate

    public TicketStatusChangeId getId() {
        return id;
    }

    public void setId(@NotNull TicketStatusChangeId id) {
        this.id = id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(@NotNull Ticket ticket) {
        this.ticket = ticket;
    }

    public Instant getChangedAt() {
        return changedAt;
    }

    public void setChangedAt(Instant changedAt) {
        this.changedAt = Objects.requireNonNull(changedAt).truncatedTo(ChronoUnit.MICROS);
    }

    public TicketStatus getStatusBefore() {
        return statusBefore;
    }

    public void setStatusBefore(@NotNull TicketStatus statusBefore) {
        this.statusBefore = statusBefore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketStatusChange that = (TicketStatusChange) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }


    //endregion

}
