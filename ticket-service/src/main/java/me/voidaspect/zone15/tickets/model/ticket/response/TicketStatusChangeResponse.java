package me.voidaspect.zone15.tickets.model.ticket.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.voidaspect.zone15.tickets.model.ticket.TicketStatus;
import me.voidaspect.zone15.tickets.model.ticket.TicketStatusChange;

import java.time.Instant;

public record TicketStatusChangeResponse(
        @JsonProperty("changedAt") Instant changedAt,
        @JsonProperty("statusBefore") TicketStatus statusBefore,
        @JsonProperty("statusAfter") TicketStatus statusAfter
) {

    public static TicketStatusChangeResponse from(TicketStatusChange source) {
        return new TicketStatusChangeResponse(
                source.getChangedAt(),
                source.getStatusBefore(),
                source.getId().getStatusAfter()
        );
    }

}
