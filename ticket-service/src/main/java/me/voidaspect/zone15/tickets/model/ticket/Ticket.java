package me.voidaspect.zone15.tickets.model.ticket;

import me.voidaspect.zone15.tickets.exception.ticket.InvalidStatusChangeException;
import me.voidaspect.zone15.tickets.exception.ticket.InvalidUsageException;
import me.voidaspect.zone15.tickets.model.holder.Holder;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"FieldMayBeFinal", "unused"})
@Entity
@Table(name = "tickets")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ticket_id_generator")
    @SequenceGenerator(name = "ticket_id_generator", sequenceName = "tickets_id_seq", allocationSize = 10)
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "holder_id", nullable = false)
    @Access(AccessType.PROPERTY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Holder holder;

    @Column(name = "status", nullable = false, columnDefinition = "int2")
    @Enumerated
    private TicketStatus status = TicketStatus.REQUESTED;

    @Column(name = "max_usages", nullable = false, updatable = false)
    private short maxUsages = 1;

    @Column(name = "created_at", nullable = false, updatable = false)
    private Instant createdAt;

    @Column(name = "activates_at", nullable = false)
    private Instant activatesAt;

    @Column(name = "expires_at")
    private Instant expiresAt;

    @Version
    private int version;

    @OrderBy("id.usageNumber")
    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TicketUsage> usages = new ArrayList<>();

    @OrderBy("id.statusAfter")
    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TicketStatusChange> statusHistory = new ArrayList<>();

    @PrePersist
    public void onPrePersist() {
        createdAt = Instant.now().truncatedTo(ChronoUnit.SECONDS);
    }

    public void addUsage(@NotNull Instant usedAt) throws InvalidUsageException {
        if (!status.canBeUsed()) {
            throw InvalidUsageException.inactive(status);
        }
        if (usedAt.isBefore(activatesAt)) {
            throw InvalidUsageException.usageBeforeActivation(usedAt, activatesAt);
        }
        if (expiresAt != null && usedAt.isAfter(expiresAt)) {
            throw InvalidUsageException.usageAfterExpiry(usedAt, expiresAt);
        }
        short usageNumber = (short) (usages.size() + 1);
        if (usageNumber > maxUsages) {
            throw InvalidUsageException.fullyUsed(maxUsages);
        } else if (usageNumber == maxUsages) {
            changeStatus(TicketStatus.USED);
        }
        var usage = new TicketUsage();
        usage.setId(new TicketUsageId(id, usageNumber));
        usage.setTicket(this);
        usage.setUsedAt(usedAt);
        usages.add(usage);
    }

    public void addStatusChange(@NotNull TicketStatus after) throws InvalidStatusChangeException {
        if (!status.canChangeTo(after)) {
            throw new InvalidStatusChangeException(status, after);
        }
        changeStatus(after);
    }

    private void changeStatus(TicketStatus after) {
        var statusChange = new TicketStatusChange();
        statusChange.setId(new TicketStatusChangeId(id, after));
        statusChange.setTicket(this);
        statusChange.setStatusBefore(status);
        statusChange.setChangedAt(Instant.now());
        status = after;
        statusHistory.add(statusChange);
    }

    // region boilerplate

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Holder getHolder() {
        return holder;
    }

    public void setHolder(@NotNull Holder holder) {
        this.holder = holder;
    }

    public TicketStatus getStatus() {
        return status;
    }

    public short getMaxUsages() {
        return maxUsages;
    }

    public void setMaxUsages(short maxUsages) {
        this.maxUsages = maxUsages;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Instant getActivatesAt() {
        return activatesAt;
    }

    public void setActivatesAt(@NotNull Instant activatesAt) {
        this.activatesAt = activatesAt.truncatedTo(ChronoUnit.MICROS);
    }

    @Nullable
    public Instant getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(@Nullable Instant expiresAt) {
        this.expiresAt = expiresAt == null ? null : expiresAt.truncatedTo(ChronoUnit.MICROS);
    }

    public int getVersion() {
        return version;
    }

    public List<TicketUsage> getUsages() {
        return usages;
    }

    public List<TicketStatusChange> getStatusHistory() {
        return statusHistory;
    }

    //endregion

}
