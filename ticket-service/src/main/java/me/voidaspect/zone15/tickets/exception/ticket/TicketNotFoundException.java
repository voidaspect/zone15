package me.voidaspect.zone15.tickets.exception.ticket;

import me.voidaspect.zone15.tickets.exception.TicketServiceException;
import org.springframework.http.HttpStatus;

public class TicketNotFoundException extends TicketServiceException {

    public TicketNotFoundException(long id) {
        super(HttpStatus.NOT_FOUND, "Ticket with id = " + id + " not found");
    }

}
