package me.voidaspect.zone15.tickets.repository;

import me.voidaspect.zone15.tickets.model.holder.Holder;
import me.voidaspect.zone15.tickets.model.holder.response.HolderResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface HolderRepository extends JpaRepository<Holder, Long> {

    @Query(value = """
            select new me.voidaspect.zone15.tickets.model.holder.response.HolderResponse(
                    h.id,
                    h.phoneNumber,
                    h.email,
                    h.firstName,
                    h.middleName,
                    h.lastName,
                    h.dateOfBirth,
                    h.gender
            ) from Holder h
            """)
    Page<HolderResponse> listAsResponses(Pageable pageable);

    @Query(value = """
            select new me.voidaspect.zone15.tickets.model.holder.response.HolderResponse(
                    h.id,
                    h.phoneNumber,
                    h.email,
                    h.firstName,
                    h.middleName,
                    h.lastName,
                    h.dateOfBirth,
                    h.gender
            ) from Holder h where h.id = ?1
            """)
    Optional<HolderResponse> findByIdAsResponse(long id);

    boolean existsByPhoneNumber(String phoneNumber);

    boolean existsByEmail(String email);

}
