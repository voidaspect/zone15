package me.voidaspect.zone15.tickets.service.holder;

import me.voidaspect.zone15.tickets.exception.holder.DuplicateHolderEmailException;
import me.voidaspect.zone15.tickets.exception.holder.DuplicateHolderPhoneNumberException;
import me.voidaspect.zone15.tickets.exception.holder.HolderNotFoundException;
import me.voidaspect.zone15.tickets.model.holder.Holder;
import me.voidaspect.zone15.tickets.model.holder.request.CreateHolderRequest;
import me.voidaspect.zone15.tickets.model.holder.request.MergeHolderRequest;
import me.voidaspect.zone15.tickets.model.holder.response.HolderResponse;
import me.voidaspect.zone15.tickets.repository.HolderRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

@Service
public class HolderService implements HolderOperations {

    private final HolderRepository holderRepository;

    public HolderService(HolderRepository holderRepository) {
        this.holderRepository = holderRepository;
    }

    @Override
    @Transactional
    public HolderResponse create(CreateHolderRequest request)
            throws DuplicateHolderEmailException, DuplicateHolderPhoneNumberException {

        var email = request.email();
        if (email != null) {
            checkEmailAvailability(email);
        }
        var phoneNumber = request.phoneNumber();
        checkPhoneNumberAvailability(phoneNumber);

        var holder = new Holder();

        holder.setPhoneNumber(phoneNumber);
        holder.setEmail(email);
        holder.setFirstName(request.firstName());
        holder.setMiddleName(request.middleName());
        holder.setLastName(request.lastName());
        holder.setDateOfBirth(request.dateOfBirth());
        var gender = request.gender();
        if (gender != null) {
            holder.setGender(gender);
        }

        return HolderResponse.from(holderRepository.save(holder));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<HolderResponse> findById(long id) {
        return holderRepository.findByIdAsResponse(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<HolderResponse> list(Pageable pageable) {
        return holderRepository.listAsResponses(pageable);
    }

    @Override
    @Transactional(rollbackFor = {DuplicateHolderPhoneNumberException.class, DuplicateHolderEmailException.class})
    public HolderResponse merge(
            long id,
            MergeHolderRequest request,
            boolean dropOptionalFields
    ) throws HolderNotFoundException, DuplicateHolderEmailException, DuplicateHolderPhoneNumberException {

        var holder = holderRepository.findById(id).orElseThrow(() -> new HolderNotFoundException(id));

        var phoneNumber = request.phoneNumber();
        boolean setPhoneNumber = false;
        if (phoneNumber != null && !phoneNumber.equals(holder.getPhoneNumber())) {
            checkPhoneNumberAvailability(phoneNumber);
            setPhoneNumber = true;
        }
        boolean setEmail = false;
        var email = request.email();
        if (email != null) {
            if (!email.equals(holder.getEmail())) {
                checkEmailAvailability(email);
                setEmail = true;
            }
        } else {
            setEmail = dropOptionalFields;
        }
        if (setPhoneNumber) {
            holder.setPhoneNumber(phoneNumber);
        }
        if (setEmail) {
            holder.setEmail(email);
        }

        var firstName = request.firstName();
        if (firstName != null && !firstName.equals(holder.getFirstName())) {
            holder.setFirstName(firstName);
        }
        var middleName = request.middleName();
        if ((dropOptionalFields || middleName != null) && !Objects.equals(middleName, holder.getMiddleName())) {
            holder.setMiddleName(middleName);
        }
        var lastName = request.lastName();
        if (lastName != null && !lastName.equals(holder.getLastName())) {
            holder.setLastName(lastName);
        }
        var dateOfBirth = request.dateOfBirth();
        if (dateOfBirth != null && !dateOfBirth.equals(holder.getDateOfBirth())) {
            holder.setDateOfBirth(dateOfBirth);
        }
        var gender = request.gender();
        if (gender != null && !gender.equals(holder.getGender())) {
            holder.setGender(gender);
        }

        return HolderResponse.from(holder);
    }

    @Override
    @Transactional
    public void delete(long id) throws HolderNotFoundException {
        var holder = holderRepository.findById(id).orElseThrow(() -> new HolderNotFoundException(id));
        holderRepository.delete(holder);
    }

    private void checkPhoneNumberAvailability(String phoneNumber) throws DuplicateHolderPhoneNumberException {
        if (holderRepository.existsByPhoneNumber(phoneNumber)) {
            throw new DuplicateHolderPhoneNumberException(phoneNumber);
        }
    }

    private void checkEmailAvailability(String email) throws DuplicateHolderEmailException {
        if (holderRepository.existsByEmail(email)) {
            throw new DuplicateHolderEmailException(email);
        }
    }

}
