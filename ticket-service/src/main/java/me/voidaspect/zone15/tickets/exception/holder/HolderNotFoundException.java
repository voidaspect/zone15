package me.voidaspect.zone15.tickets.exception.holder;

import me.voidaspect.zone15.tickets.exception.TicketServiceException;
import org.springframework.http.HttpStatus;

public class HolderNotFoundException extends TicketServiceException {

    public HolderNotFoundException(long id) {
        super(HttpStatus.NOT_FOUND, "Ticket holder with id = " + id + " not found");
    }

}
