package me.voidaspect.zone15.tickets.repository;

import me.voidaspect.zone15.tickets.model.ticket.Ticket;
import me.voidaspect.zone15.tickets.model.ticket.response.TicketResponse;
import me.voidaspect.zone15.tickets.model.ticket.response.TicketStatusChangeResponse;
import me.voidaspect.zone15.tickets.model.ticket.response.TicketUsageResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TicketRepository extends JpaRepository<Ticket, Long> {

    @Query(value = """
            select new me.voidaspect.zone15.tickets.model.ticket.response.TicketResponse(
                    t.id,
                    t.status,
                    t.maxUsages,
                    t.createdAt,
                    t.activatesAt,
                    t.expiresAt,
                    t.holder.id
            ) from Ticket t where t.id = ?1
            """)
    Optional<TicketResponse> findByIdAsResponse(long id);

    @Query(value = """
            select new me.voidaspect.zone15.tickets.model.ticket.response.TicketResponse(
                    t.id,
                    t.status,
                    t.maxUsages,
                    t.createdAt,
                    t.activatesAt,
                    t.expiresAt,
                    t.holder.id
            ) from Ticket t where t.holder.id = ?1
            """)
    Page<TicketResponse> findByHolderIdAsResponse(long holderId, Pageable pageable);

    @Query(value = """
            select new me.voidaspect.zone15.tickets.model.ticket.response.TicketStatusChangeResponse(
                    tsc.changedAt,
                    tsc.statusBefore,
                    tsc.id.statusAfter
            ) from TicketStatusChange tsc where tsc.ticket.id = ?1 order by tsc.id.statusAfter
            """)
    List<TicketStatusChangeResponse> findStatusHistoryByTicketId(long ticketId);

    @Query(value = """
            select new me.voidaspect.zone15.tickets.model.ticket.response.TicketUsageResponse(
                    tu.id.usageNumber,
                    tu.usedAt
            ) from TicketUsage tu where tu.ticket.id = ?1 order by tu.id.usageNumber
            """)
    List<TicketUsageResponse> findUsageHistoryByTicketId(long ticketId);

}
