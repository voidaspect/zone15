package me.voidaspect.zone15.tickets.model;

import java.util.Optional;

/**
 *
 * @param <T> type
 */
public interface Window<T extends Comparable<? super T>> {

    Optional<T> start();

    Optional<T> finish();

}
