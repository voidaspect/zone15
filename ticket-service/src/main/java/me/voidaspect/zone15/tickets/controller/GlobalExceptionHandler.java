package me.voidaspect.zone15.tickets.controller;

import me.voidaspect.zone15.tickets.exception.TicketServiceException;
import me.voidaspect.zone15.tickets.model.error.BindingErrorErrorResponse;
import me.voidaspect.zone15.tickets.model.error.ErrorResponse;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(TicketServiceException.class)
    public ResponseEntity<ErrorResponse> handleAnyServiceException(TicketServiceException ex,
                                                                   HttpServletRequest request) {
        return ResponseEntity
                .status(ex.getStatus())
                .body(ErrorResponse.from(ex, request));
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorResponse> handleDataIntegrityViolation(DataIntegrityViolationException ex,
                                                                      HttpServletRequest request) {
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(ErrorResponse.from(ex, request));
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleUnspecifiedException(Exception ex, HttpServletRequest request) {
        return ErrorResponse.from(ex, request);
    }

    @Override
    @NotNull
    protected ResponseEntity<Object> handleBindException(@NotNull BindException ex,
                                                         @NotNull HttpHeaders headers,
                                                         @NotNull HttpStatus status,
                                                         @NotNull WebRequest request) {

        var body = BindingErrorErrorResponse.from(ex, getRequest(request));
        return handleExceptionInternal(ex, body, headers, status, request);
    }

    @Override
    @NotNull
    protected ResponseEntity<Object> handleMethodArgumentNotValid(@NotNull MethodArgumentNotValidException ex,
                                                                  @NotNull HttpHeaders headers,
                                                                  @NotNull HttpStatus status,
                                                                  @NotNull WebRequest request) {

        return handleBindException(ex, headers, status, request);
    }

    @Override
    @NotNull
    protected ResponseEntity<Object> handleExceptionInternal(@NotNull Exception ex,
                                                             @Nullable Object body,
                                                             @NotNull HttpHeaders headers,
                                                             @NotNull HttpStatus status,
                                                             @NotNull WebRequest request) {
        body = Objects.requireNonNullElseGet(body,
                () -> ErrorResponse.from(ex, getRequest(request)));

        return super.handleExceptionInternal(ex, body, headers, status, request);
    }

    private static HttpServletRequest getRequest(WebRequest request) {
        return ((ServletWebRequest) request).getRequest();
    }
}
