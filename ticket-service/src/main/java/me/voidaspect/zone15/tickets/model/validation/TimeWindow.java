package me.voidaspect.zone15.tickets.model.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target({TYPE, METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Documented
@Constraint(validatedBy = TimeWindowValidator.class)
public @interface TimeWindow {

    String message() default "{me.voidaspect.zone15.tickets.model.validation.TimeWindow.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
