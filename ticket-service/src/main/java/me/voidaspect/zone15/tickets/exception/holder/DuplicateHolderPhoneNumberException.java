package me.voidaspect.zone15.tickets.exception.holder;

import me.voidaspect.zone15.tickets.exception.TicketServiceException;
import org.springframework.http.HttpStatus;

public class DuplicateHolderPhoneNumberException extends TicketServiceException {

    public DuplicateHolderPhoneNumberException(String phoneNumber) {
        super(HttpStatus.CONFLICT, "Phone number (" + phoneNumber + ") is already taken");
    }

}
