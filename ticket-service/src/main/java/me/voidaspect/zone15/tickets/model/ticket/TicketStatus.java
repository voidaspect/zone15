package me.voidaspect.zone15.tickets.model.ticket;

import org.jetbrains.annotations.NotNull;

public enum TicketStatus {

    REQUESTED,

    CONFIRMED,

    REJECTED,

    CANCELLED,

    REVOKED,

    USED;

    public boolean canChangeTo(@NotNull TicketStatus after) {
        return switch (after) {
            case REQUESTED, USED -> false;
            case CONFIRMED, REJECTED -> this == REQUESTED;
            case CANCELLED, REVOKED -> this == CONFIRMED;
        };
    }

    public boolean canBeUsed() {
        return this == CONFIRMED;
    }

}
