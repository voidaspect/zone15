package me.voidaspect.zone15.tickets.model.holder.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.voidaspect.zone15.tickets.Routes;
import me.voidaspect.zone15.tickets.model.ResourceResponse;

public record HolderTicketsReference(long holderId) implements ResourceResponse {

    @Override
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String path() {
        return Routes.holderTickets(holderId);
    }

}
