package me.voidaspect.zone15.tickets.service.ticket;

import me.voidaspect.zone15.tickets.exception.holder.HolderNotFoundException;
import me.voidaspect.zone15.tickets.exception.ticket.InvalidStatusChangeException;
import me.voidaspect.zone15.tickets.exception.ticket.InvalidUsageException;
import me.voidaspect.zone15.tickets.exception.ticket.TicketNotFoundException;
import me.voidaspect.zone15.tickets.model.ticket.Ticket;
import me.voidaspect.zone15.tickets.model.ticket.request.AddTicketUsageRequest;
import me.voidaspect.zone15.tickets.model.ticket.request.CreateTicketRequest;
import me.voidaspect.zone15.tickets.model.ticket.request.MergeTicketRequest;
import me.voidaspect.zone15.tickets.model.ticket.response.DetailedTicketResponse;
import me.voidaspect.zone15.tickets.model.ticket.response.TicketHistoryResponse;
import me.voidaspect.zone15.tickets.model.ticket.response.TicketResponse;
import me.voidaspect.zone15.tickets.repository.HolderRepository;
import me.voidaspect.zone15.tickets.repository.TicketRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
public class TicketService implements TicketOperations {

    private final TicketRepository ticketRepository;

    private final HolderRepository holderRepository;

    public TicketService(
            TicketRepository ticketRepository,
            HolderRepository holderRepository
    ) {
        this.ticketRepository = ticketRepository;
        this.holderRepository = holderRepository;
    }

    @Override
    @Transactional
    public TicketResponse create(
            long holderId,
            CreateTicketRequest request
    ) throws HolderNotFoundException {

        if (!holderRepository.existsById(holderId)) {
            throw new HolderNotFoundException(holderId);
        }

        var holder = holderRepository.getOne(holderId);

        var ticket = new Ticket();

        ticket.setHolder(holder);
        var maxUsages = request.maxUsages();
        if (maxUsages != null) {
            ticket.setMaxUsages(request.maxUsages());
        }
        ticket.setActivatesAt(request.activatesAt().toInstant());
        var expiresAt = request.expiresAt();
        if (expiresAt != null) {
            ticket.setExpiresAt(expiresAt.toInstant());
        }

        return TicketResponse.from(ticketRepository.save(ticket));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TicketResponse> findByHolderId(long holderId, Pageable pageable) throws HolderNotFoundException {
        if (!holderRepository.existsById(holderId)) {
            throw new HolderNotFoundException(holderId);
        }
        return ticketRepository.findByHolderIdAsResponse(holderId, pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DetailedTicketResponse> findById(long id) {
        return ticketRepository.findByIdAsResponse(id).map(ticket -> {
            var usages = ticketRepository.findUsageHistoryByTicketId(id);
            var statusChanges = ticketRepository.findStatusHistoryByTicketId(id);
            return new DetailedTicketResponse(ticket, new TicketHistoryResponse(usages, statusChanges));
        });
    }

    @Override
    @Transactional(rollbackFor = InvalidStatusChangeException.class)
    public DetailedTicketResponse merge(
            long id,
            MergeTicketRequest request,
            boolean dropOptionalFields
    ) throws TicketNotFoundException, InvalidStatusChangeException {

        var ticket = ticketRepository.findById(id).orElseThrow(() -> new TicketNotFoundException(id));

        var status = request.status();
        if (status != null) {
            ticket.addStatusChange(status);
        }

        var activatesAt = request.activatesAt();
        if (activatesAt != null) {
            var activatesInstant = activatesAt.toInstant().truncatedTo(ChronoUnit.MICROS);
            if (!activatesInstant.equals(ticket.getActivatesAt())) {
                ticket.setActivatesAt(activatesInstant);
            }
        }

        var expiresAt = request.expiresAt();
        if (expiresAt != null) {
            var expiresInstant = expiresAt.toInstant().truncatedTo(ChronoUnit.MICROS);
            if (!expiresInstant.equals(ticket.getExpiresAt())) {
                ticket.setExpiresAt(expiresInstant);
            }
        } else if (dropOptionalFields) {
            ticket.setExpiresAt(null);
        }

        return DetailedTicketResponse.from(ticket);
    }

    @Override
    @Transactional(rollbackFor = InvalidUsageException.class)
    public DetailedTicketResponse addUsage(
            long id,
            AddTicketUsageRequest request
    ) throws TicketNotFoundException, InvalidUsageException {
        var ticket = ticketRepository.findById(id).orElseThrow(() -> new TicketNotFoundException(id));
        ticket.addUsage(request.usedAt().toInstant());
        return DetailedTicketResponse.from(ticket);
    }

}
