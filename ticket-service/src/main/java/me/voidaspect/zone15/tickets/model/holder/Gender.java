package me.voidaspect.zone15.tickets.model.holder;

public enum Gender {
    MALE,
    FEMALE,
    UNSPECIFIED
}
