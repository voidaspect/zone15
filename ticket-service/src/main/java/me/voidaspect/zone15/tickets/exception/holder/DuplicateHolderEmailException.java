package me.voidaspect.zone15.tickets.exception.holder;

import me.voidaspect.zone15.tickets.exception.TicketServiceException;
import org.springframework.http.HttpStatus;

public class DuplicateHolderEmailException extends TicketServiceException {

    public DuplicateHolderEmailException(String email) {
        super(HttpStatus.CONFLICT, "Email address (" + email + ") is already taken");
    }

}
