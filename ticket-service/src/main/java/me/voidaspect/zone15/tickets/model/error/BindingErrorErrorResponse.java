package me.voidaspect.zone15.tickets.model.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.validation.BindException;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public record BindingErrorErrorResponse(
        @JsonProperty("type")
        String type,

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty("reasons")
        List<DataBindingReason> reason,

        @JsonProperty("timestamp")
        Instant timestamp,

        @JsonProperty("path")
        String path
) {

    public static BindingErrorErrorResponse from(BindException error, HttpServletRequest request) {
        var allErrors = error.getBindingResult().getAllErrors();
        var errorCount = allErrors.size();
        var reasons = allErrors.stream()
                .flatMap(objectError -> DataBindingReason.from(objectError).stream())
                .collect(Collectors.toCollection(() -> new ArrayList<>(errorCount)));

        return new BindingErrorErrorResponse(
                error.getClass().getName(),
                reasons,
                Instant.now(),
                request.getRequestURI()
        );
    }
}
