package me.voidaspect.zone15.tickets.model.ticket.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.ZonedDateTime;

public record AddTicketUsageRequest(
        @JsonProperty("usedAt")
        @PastOrPresent
        @NotNull
        ZonedDateTime usedAt
) {
}
