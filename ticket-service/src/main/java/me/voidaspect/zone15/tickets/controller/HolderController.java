package me.voidaspect.zone15.tickets.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import me.voidaspect.zone15.tickets.Routes;
import me.voidaspect.zone15.tickets.config.springdoc.ConstraintViolationApiResponse;
import me.voidaspect.zone15.tickets.config.springdoc.HolderNotFoundApiResponse;
import me.voidaspect.zone15.tickets.config.springdoc.InvalidInputApiResponse;
import me.voidaspect.zone15.tickets.exception.holder.DuplicateHolderEmailException;
import me.voidaspect.zone15.tickets.exception.holder.DuplicateHolderPhoneNumberException;
import me.voidaspect.zone15.tickets.exception.holder.HolderNotFoundException;
import me.voidaspect.zone15.tickets.model.holder.request.CreateHolderRequest;
import me.voidaspect.zone15.tickets.model.holder.request.MergeHolderRequest;
import me.voidaspect.zone15.tickets.model.holder.response.HolderResponse;
import me.voidaspect.zone15.tickets.model.ticket.request.CreateTicketRequest;
import me.voidaspect.zone15.tickets.model.ticket.response.TicketResponse;
import me.voidaspect.zone15.tickets.service.holder.HolderOperations;
import me.voidaspect.zone15.tickets.service.ticket.TicketOperations;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;

@RestController
@RequestMapping(Routes.HOLDERS)
public class HolderController {

    private final HolderOperations holders;

    private final TicketOperations tickets;

    public HolderController(HolderOperations holders, TicketOperations tickets) {
        this.holders = holders;
        this.tickets = tickets;
    }

    @Operation(summary = "Create Holder")
    @ApiResponse(responseCode = "201", description = "Holder created",
            content = @Content(schema = @Schema(implementation = HolderResponse.class)))
    @InvalidInputApiResponse
    @ConstraintViolationApiResponse
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<HolderResponse> create(
            @Valid @RequestBody CreateHolderRequest request,
            UriComponentsBuilder ucb
    ) throws DuplicateHolderPhoneNumberException, DuplicateHolderEmailException {

        var response = holders.create(request);
        var location = ucb.path(response.path()).build();

        return ResponseEntity.created(location.toUri()).body(response);
    }

    @Operation(summary = "Find Holder by id")
    @ApiResponse(responseCode = "200", description = "Holder found",
            content = @Content(schema = @Schema(implementation = HolderResponse.class)))
    @HolderNotFoundApiResponse
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public HolderResponse findById(@PathVariable long id) throws HolderNotFoundException {
        return holders.findById(id)
                .orElseThrow(() -> new HolderNotFoundException(id));
    }

    @Operation(summary = "Retrieve a page of Holders")
    @ApiResponse(responseCode = "200", description = "Holders retrieved",
            content = @Content(schema = @Schema(implementation = HolderResponsePage.class)))
    @PageableAsQueryParam
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<HolderResponse> list(@Parameter(hidden = true) Pageable pageable) {
        return holders.list(pageable);
    }

    @Operation(summary = "Merge Holder by id")
    @ApiResponse(responseCode = "200", description = "Holder merged",
            content = @Content(schema = @Schema(implementation = HolderResponse.class)))
    @InvalidInputApiResponse
    @HolderNotFoundApiResponse
    @ConstraintViolationApiResponse
    @PatchMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public HolderResponse merge(
            @PathVariable long id,
            @Valid @RequestBody MergeHolderRequest request,
            @RequestParam(name = "drop-optional-fields", defaultValue = "false") boolean dropOptionalFields
    ) throws DuplicateHolderEmailException, DuplicateHolderPhoneNumberException, HolderNotFoundException {

        return holders.merge(id, request, dropOptionalFields);
    }

    @Operation(summary = "Delete Holder by id")
    @ApiResponse(responseCode = "204", description = "Holder deleted")
    @InvalidInputApiResponse
    @HolderNotFoundApiResponse
    @ConstraintViolationApiResponse
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) throws HolderNotFoundException {

        holders.delete(id);
    }

    @Operation(summary = "Create Ticket for Holder")
    @ApiResponse(responseCode = "201", description = "Ticket created",
            content = @Content(schema = @Schema(implementation = TicketResponse.class)))
    @HolderNotFoundApiResponse
    @PostMapping(
            value = "/{id}/tickets",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<TicketResponse> addTicket(
            @PathVariable long id,
            @RequestBody @Valid CreateTicketRequest request,
            UriComponentsBuilder ucb
    ) throws HolderNotFoundException {
        var response = tickets.create(id, request);
        var location = ucb.path(response.path()).build();
        return ResponseEntity.created(location.toUri()).body(response);
    }

    @Operation(summary = "Retrieve a page of Tickets by Holder id")
    @ApiResponse(
            responseCode = "200", description = "Tickets retrieved",
            content = @Content(schema = @Schema(implementation = TicketResponsePage.class)))
    @HolderNotFoundApiResponse
    @PageableAsQueryParam
    @GetMapping(value = "/{id}/tickets", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<TicketResponse> findTickets(
            @PathVariable long id,
            @Parameter(hidden = true) Pageable pageable
    ) throws HolderNotFoundException {

        return tickets.findByHolderId(id, pageable);
    }

    // region workaround for lack of generic type support in springdoc @ApiResponse
    private interface HolderResponsePage extends Page<HolderResponse> {
    }

    private interface TicketResponsePage extends Page<TicketResponse> {
    }
    // endregion

}
