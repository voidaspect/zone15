package me.voidaspect.zone15.tickets.exception.ticket;

import me.voidaspect.zone15.tickets.exception.TicketServiceException;
import me.voidaspect.zone15.tickets.model.ticket.TicketStatus;
import org.springframework.http.HttpStatus;

public class InvalidStatusChangeException extends TicketServiceException {

    public InvalidStatusChangeException(TicketStatus before, TicketStatus after) {
        super(HttpStatus.CONFLICT, "Can't manually change status from " + before + " to " + after);
    }
}
