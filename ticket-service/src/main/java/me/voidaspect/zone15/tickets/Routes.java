package me.voidaspect.zone15.tickets;

public final class Routes {

    private Routes() {
    }

    public static final String API_ROOT = "/api";

    public static final String HOLDERS = API_ROOT + "/holders";

    public static final String TICKETS = API_ROOT + "/tickets";

    public static String holder(long id) {
        return HOLDERS + '/' + id;
    }

    public static String holderTickets(long holderId) {
        return holder(holderId) + "/tickets";
    }

    public static String ticket(long id) {
        return TICKETS + '/' + id;
    }

}
