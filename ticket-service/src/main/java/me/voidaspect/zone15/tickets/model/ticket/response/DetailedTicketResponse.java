package me.voidaspect.zone15.tickets.model.ticket.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import me.voidaspect.zone15.tickets.model.ticket.Ticket;

public record DetailedTicketResponse(
        @JsonUnwrapped TicketResponse ticket,
        @JsonProperty("history") TicketHistoryResponse history
) {

    public static DetailedTicketResponse from(Ticket ticket) {
        return new DetailedTicketResponse(
                TicketResponse.from(ticket),
                TicketHistoryResponse.from(ticket)
        );
    }

}
