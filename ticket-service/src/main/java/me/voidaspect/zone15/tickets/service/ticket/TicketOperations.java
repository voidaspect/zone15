package me.voidaspect.zone15.tickets.service.ticket;

import me.voidaspect.zone15.tickets.exception.holder.HolderNotFoundException;
import me.voidaspect.zone15.tickets.exception.ticket.InvalidStatusChangeException;
import me.voidaspect.zone15.tickets.exception.ticket.InvalidUsageException;
import me.voidaspect.zone15.tickets.exception.ticket.TicketNotFoundException;
import me.voidaspect.zone15.tickets.model.ticket.request.AddTicketUsageRequest;
import me.voidaspect.zone15.tickets.model.ticket.request.CreateTicketRequest;
import me.voidaspect.zone15.tickets.model.ticket.request.MergeTicketRequest;
import me.voidaspect.zone15.tickets.model.ticket.response.DetailedTicketResponse;
import me.voidaspect.zone15.tickets.model.ticket.response.TicketResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface TicketOperations {

    TicketResponse create(
            long holderId,
            CreateTicketRequest request
    ) throws HolderNotFoundException;

    Page<TicketResponse> findByHolderId(
            long holderId,
            Pageable pageable
    ) throws HolderNotFoundException;

    Optional<DetailedTicketResponse> findById(long id);

    DetailedTicketResponse merge(
            long id,
            MergeTicketRequest request,
            boolean dropOptionalFields
    ) throws TicketNotFoundException, InvalidStatusChangeException;

    DetailedTicketResponse addUsage(
            long id,
            AddTicketUsageRequest request
    ) throws TicketNotFoundException, InvalidUsageException;

}
