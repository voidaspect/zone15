package me.voidaspect.zone15.tickets.model.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_NULL)
public record DataBindingReason(
        @JsonProperty("property")
        @Nullable
        String property,

        @JsonProperty("rejectedValue")
        @Nullable
        Object rejectedValue,

        @JsonProperty("info")
        @Nullable
        String info
) {

    public static Optional<DataBindingReason> from(ObjectError error) {
        if (error == null) {
            return Optional.empty();
        }
        String info = error.getDefaultMessage();
        Object rejectedValue = null;
        String property = null;
        if (error instanceof FieldError fieldError) {
            rejectedValue = fieldError.getRejectedValue();
            property = fieldError.getField();
        }
        if (info == null && rejectedValue == null && property == null) {
            return Optional.empty();
        }
        return Optional.of(new DataBindingReason(property, rejectedValue, info));
    }
}
