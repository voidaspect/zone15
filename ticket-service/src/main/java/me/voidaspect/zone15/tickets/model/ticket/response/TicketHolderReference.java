package me.voidaspect.zone15.tickets.model.ticket.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.voidaspect.zone15.tickets.Routes;
import me.voidaspect.zone15.tickets.model.ResourceResponse;

public record TicketHolderReference(@JsonProperty(value = "id") long id) implements ResourceResponse {

    @Override
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String path() {
        return Routes.holder(id);
    }

}
