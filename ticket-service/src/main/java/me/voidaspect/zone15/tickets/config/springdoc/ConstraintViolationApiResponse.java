package me.voidaspect.zone15.tickets.config.springdoc;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import me.voidaspect.zone15.tickets.model.error.ErrorResponse;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
@ApiResponse(responseCode = "409", description = "Data integrity constraint violation",
        content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
public @interface ConstraintViolationApiResponse {
}
