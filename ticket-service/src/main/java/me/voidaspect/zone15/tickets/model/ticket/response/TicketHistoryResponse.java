package me.voidaspect.zone15.tickets.model.ticket.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.voidaspect.zone15.tickets.model.ticket.Ticket;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public record TicketHistoryResponse(
        @JsonProperty("usages") Collection<TicketUsageResponse> usages,
        @JsonProperty("statusChanges") Collection<TicketStatusChangeResponse> statusChanges
) {

    public static TicketHistoryResponse from(Ticket source) {
        var usages = source.getUsages();
        var statusChanges = source.getStatusHistory();
        return new TicketHistoryResponse(
                usages.stream()
                        .map(TicketUsageResponse::from)
                        .collect(Collectors.toCollection(() -> new ArrayList<>(usages.size()))),
                statusChanges.stream()
                        .map(TicketStatusChangeResponse::from)
                        .collect(Collectors.toCollection(() -> new ArrayList<>(statusChanges.size())))
        );
    }
}
