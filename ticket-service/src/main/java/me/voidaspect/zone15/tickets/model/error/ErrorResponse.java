package me.voidaspect.zone15.tickets.model.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

public record ErrorResponse(
        @JsonProperty("type")
        String type,

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty("reason")
        String reason,

        @JsonProperty("timestamp")
        Instant timestamp,

        @JsonProperty("path")
        String path
) {

    public static ErrorResponse from(Throwable error, HttpServletRequest request) {
        return new ErrorResponse(
                error.getClass().getName(),
                error.getLocalizedMessage(),
                Instant.now(),
                request.getRequestURI()
        );
    }

}
