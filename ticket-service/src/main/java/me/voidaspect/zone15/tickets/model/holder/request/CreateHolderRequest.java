package me.voidaspect.zone15.tickets.model.holder.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.voidaspect.zone15.tickets.model.holder.Gender;
import me.voidaspect.zone15.tickets.model.validation.NullableNotBlank;
import me.voidaspect.zone15.tickets.model.validation.PhoneNumber;

import javax.validation.constraints.*;
import java.time.LocalDate;

public record CreateHolderRequest(
        @JsonProperty("phoneNumber") @PhoneNumber @NotNull String phoneNumber,
        @JsonProperty("email") @Email String email,
        @JsonProperty("firstName") @NotBlank String firstName,
        @JsonProperty("middleName") @NullableNotBlank String middleName,
        @JsonProperty("lastName") @NotBlank String lastName,
        @JsonProperty("dateOfBirth") @Past @NotNull LocalDate dateOfBirth,
        @JsonProperty("gender") Gender gender
) {
}
