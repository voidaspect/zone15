package me.voidaspect.zone15.tickets.model.ticket;

import org.hibernate.annotations.Immutable;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

@Entity
@Table(name = "ticket_usage_history")
@Immutable
public class TicketUsage {

    @EmbeddedId
    private TicketUsageId id;

    @MapsId("ticketId")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Ticket ticket;

    @Column(name = "used_at", nullable = false)
    private Instant usedAt;

    //region boilerplate

    public TicketUsageId getId() {
        return id;
    }

    public void setId(TicketUsageId id) {
        this.id = id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(@NotNull Ticket ticket) {
        this.ticket = ticket;
    }

    public Instant getUsedAt() {
        return usedAt;
    }

    public void setUsedAt(@NotNull Instant usedAt) {
        this.usedAt = usedAt.truncatedTo(ChronoUnit.MICROS);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketUsage that = (TicketUsage) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    //endregion
}
