package me.voidaspect.zone15.tickets.model.ticket;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TicketUsageId implements Serializable {

    @Column(nullable = false, updatable = false)
    private long ticketId;

    @Column(nullable = false, updatable = false)
    private short usageNumber;

    public TicketUsageId() {
    }

    public TicketUsageId(long ticketId, short usageNumber) {
        this.ticketId = ticketId;
        this.usageNumber = usageNumber;
    }

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public short getUsageNumber() {
        return usageNumber;
    }

    public void setUsageNumber(short usageNumber) {
        this.usageNumber = usageNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TicketUsageId that = (TicketUsageId) o;

        return usageNumber == that.usageNumber && ticketId != that.ticketId;
    }

    @Override
    public int hashCode() {
        int result = Long.hashCode(ticketId);
        result = 31 * result + Short.hashCode(usageNumber);
        return result;
    }
}
