package me.voidaspect.zone15.tickets.exception.ticket;

import me.voidaspect.zone15.tickets.exception.TicketServiceException;
import me.voidaspect.zone15.tickets.model.ticket.TicketStatus;
import org.springframework.http.HttpStatus;

import java.time.Instant;

public class InvalidUsageException extends TicketServiceException {

    private InvalidUsageException(HttpStatus status, String message) {
        super(status, message);
    }

    public static InvalidUsageException usageBeforeActivation(Instant usedAt, Instant activatesAt) {
        return new InvalidUsageException(HttpStatus.CONFLICT,
                "Can't add new ticket usage with usedAt (" + usedAt + ") before activation time + (" + activatesAt + ')');
    }

    public static InvalidUsageException usageAfterExpiry(Instant usedAt, Instant expiresAt) {
        return new InvalidUsageException(HttpStatus.CONFLICT,
                "Can't add new ticket usage with usedAt (" + usedAt + ") after expiration time + (" + expiresAt + ')');
    }

    public static InvalidUsageException fullyUsed(short maxUsages) {
        return new InvalidUsageException(HttpStatus.CONFLICT,
                "Ticket was already used max amount of times (" + maxUsages + ')');
    }

    public static InvalidUsageException inactive(TicketStatus status) {
        return new InvalidUsageException(HttpStatus.CONFLICT,
                "Can't use ticket with status " + status);
    }
}
