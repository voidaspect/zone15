package me.voidaspect.zone15.tickets.exception;

import org.springframework.http.HttpStatus;

public class TicketServiceException extends Exception {

    private final HttpStatus status;

    public TicketServiceException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    public TicketServiceException(HttpStatus status, String message, Throwable cause) {
        super(message, cause);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
