package me.voidaspect.zone15.tickets.model.holder.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.voidaspect.zone15.tickets.model.holder.Gender;
import me.voidaspect.zone15.tickets.model.validation.NullableNotBlank;
import me.voidaspect.zone15.tickets.model.validation.PhoneNumber;

import javax.validation.constraints.Email;
import javax.validation.constraints.Past;
import java.time.LocalDate;

public record MergeHolderRequest(
        @JsonProperty("phoneNumber") @PhoneNumber String phoneNumber,
        @JsonProperty("email") @Email String email,
        @JsonProperty("firstName") @NullableNotBlank String firstName,
        @JsonProperty("middleName") @NullableNotBlank String middleName,
        @JsonProperty("lastName") @NullableNotBlank String lastName,
        @JsonProperty("dateOfBirth") @Past LocalDate dateOfBirth,
        @JsonProperty("gender") Gender gender
) {
}
