package me.voidaspect.zone15.tickets;

import org.junit.jupiter.api.Tag;
import org.springframework.test.context.ActiveProfiles;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Tag(TestProfiles.INTEGRATION_TEST)
@ActiveProfiles(TestProfiles.INTEGRATION_TEST)
public @interface IntegrationTest {
}
