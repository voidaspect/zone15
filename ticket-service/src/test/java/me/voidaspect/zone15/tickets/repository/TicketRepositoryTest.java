package me.voidaspect.zone15.tickets.repository;

import me.voidaspect.zone15.tickets.CleanDB;
import me.voidaspect.zone15.tickets.TicketServicePersistenceTest;
import me.voidaspect.zone15.tickets.model.holder.Holder;
import me.voidaspect.zone15.tickets.model.ticket.Ticket;
import me.voidaspect.zone15.tickets.model.ticket.TicketStatus;
import me.voidaspect.zone15.tickets.model.ticket.TicketStatusChange;
import me.voidaspect.zone15.tickets.model.ticket.TicketUsage;
import me.voidaspect.zone15.tickets.model.ticket.response.TicketResponse;
import me.voidaspect.zone15.tickets.model.ticket.response.TicketStatusChangeResponse;
import me.voidaspect.zone15.tickets.model.ticket.response.TicketUsageResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

@CleanDB
class TicketRepositoryTest extends TicketServicePersistenceTest {

    @Autowired
    private TicketRepository ticketRepository;

    @Test
    void testFindByHolderIdAsResponse() {
        var holder = persistTestHolder();

        var ticket1 = testTicket(holder);
        var now = Instant.now();
        ticket1.setActivatesAt(now);
        ticket1.setExpiresAt(now.plus(2, ChronoUnit.DAYS));

        var ticket2 = testTicket(holder);
        ticket2.setActivatesAt(now.plus(1, ChronoUnit.DAYS));
        ticket2.setMaxUsages((short) 3);

        ticketRepository.save(ticket1);
        ticketRepository.save(ticket2);

        assertThat(ticket1.getCreatedAt()).isNotNull();
        assertThat(ticket2.getCreatedAt()).isNotNull();

        var ticketResponses = ticketRepository
                .findByHolderIdAsResponse(holder.getId(), PageRequest.of(0, 2, Sort.by("id")));

        assertThat(ticketResponses.getTotalElements()).isEqualTo(2);
        assertThat(ticketResponses.getNumberOfElements()).isEqualTo(2);
        assertThat(ticketResponses.getContent().get(0))
                .satisfies(response -> assertResponseMatchesTicket(ticket1, response));
        assertThat(ticketResponses.getContent().get(1))
                .satisfies(response -> assertResponseMatchesTicket(ticket2, response));
    }

    @Test
    void testFindByIdAsResponse() {
        var holder = persistTestHolder();

        var ticket = testTicket(holder);
        var now = Instant.now();
        ticket.setMaxUsages((short) 2);
        ticket.setActivatesAt(now.minus(3, ChronoUnit.HOURS));
        ticket.setExpiresAt(now.plus(2, ChronoUnit.DAYS));

        ticketRepository.save(ticket);

        assertThatCode(() -> ticket.addStatusChange(TicketStatus.CONFIRMED))
                .doesNotThrowAnyException();
        assertThatCode(() -> ticket.addUsage(Instant.now()))
                .doesNotThrowAnyException();
        assertThatCode(() -> ticket.addUsage(Instant.now()))
                .doesNotThrowAnyException();

        assertThat(ticket.getStatus()).isSameAs(TicketStatus.USED);
        assertThat(ticket.getStatusHistory()).hasSize(2);
        assertThat(ticket.getStatusHistory().get(0).getStatusBefore()).isEqualTo(TicketStatus.REQUESTED);
        assertThat(ticket.getStatusHistory().get(0).getId().getStatusAfter()).isEqualTo(TicketStatus.CONFIRMED);
        assertThat(ticket.getStatusHistory().get(0).getId().getTicketId()).isEqualTo(ticket.getId());
        assertThat(ticket.getStatusHistory().get(0).getTicket()).isEqualTo(ticket);
        assertThat(ticket.getStatusHistory().get(1).getStatusBefore()).isEqualTo(TicketStatus.CONFIRMED);
        assertThat(ticket.getStatusHistory().get(1).getId().getStatusAfter()).isEqualTo(TicketStatus.USED);
        assertThat(ticket.getStatusHistory().get(1).getId().getTicketId()).isEqualTo(ticket.getId());
        assertThat(ticket.getStatusHistory().get(1).getTicket()).isEqualTo(ticket);
        assertThat(ticket.getUsages()).hasSize(2);

        var ticketResponse = ticketRepository.findByIdAsResponse(ticket.getId());
        var usageHistoryResponses = ticketRepository
                .findUsageHistoryByTicketId(ticket.getId());
        var statusHistoryResponses = ticketRepository
                .findStatusHistoryByTicketId(ticket.getId());

        assertThat(ticketResponse).hasValueSatisfying(response -> assertResponseMatchesTicket(ticket, response));
        assertResponseMatchesUsageHistory(ticket.getUsages().get(0), usageHistoryResponses.get(0));
        assertResponseMatchesUsageHistory(ticket.getUsages().get(1), usageHistoryResponses.get(1));
        assertResponseMatchesStatusHistory(ticket.getStatusHistory().get(0), statusHistoryResponses.get(0));
        assertResponseMatchesStatusHistory(ticket.getStatusHistory().get(1), statusHistoryResponses.get(1));
    }

    private Holder persistTestHolder() {
        var holder = testHolder1();
        entityManager.persist(holder);
        return holder;
    }

    private static void assertResponseMatchesTicket(Ticket ticket, TicketResponse response) {
        assertThat(response.id()).isEqualTo(ticket.getId());
        assertThat(response.activatesAt()).isEqualTo(ticket.getActivatesAt());
        assertThat(response.expiresAt()).isEqualTo(ticket.getExpiresAt());
        assertThat(response.createdAt()).isEqualTo(ticket.getCreatedAt());
        assertThat(response.maxUsages()).isEqualTo(ticket.getMaxUsages());
        assertThat(response.status()).isEqualTo(ticket.getStatus());
    }

    private static void assertResponseMatchesUsageHistory(TicketUsage ticketUsage, TicketUsageResponse response) {
        assertThat(response.usageNumber()).isEqualTo(ticketUsage.getId().getUsageNumber());
        assertThat(response.usedAt()).isEqualTo(ticketUsage.getUsedAt());
    }

    private static void assertResponseMatchesStatusHistory(TicketStatusChange ticketStatusChange,
                                                           TicketStatusChangeResponse response) {
        assertThat(response.changedAt()).isEqualTo(ticketStatusChange.getChangedAt());
        assertThat(response.statusBefore()).isEqualTo(ticketStatusChange.getStatusBefore());
        assertThat(response.statusAfter()).isEqualTo(ticketStatusChange.getId().getStatusAfter());
    }
}