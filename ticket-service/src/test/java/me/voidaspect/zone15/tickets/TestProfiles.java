package me.voidaspect.zone15.tickets;

public final class TestProfiles {

    private TestProfiles() {
    }

    public static final String INTEGRATION_TEST = "integration-test";

}
