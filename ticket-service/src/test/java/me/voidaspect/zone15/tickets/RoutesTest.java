package me.voidaspect.zone15.tickets;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RoutesTest {

    @Test
    void testRoutes() {
        assertEquals("/api", Routes.API_ROOT);
        assertEquals("/api/tickets", Routes.TICKETS);
        assertEquals("/api/holders", Routes.HOLDERS);

        long id = 1_000_000_000_000L;

        assertEquals("/api/tickets/1000000000000", Routes.ticket(id));
        assertEquals("/api/holders/1000000000000", Routes.holder(id));
        assertEquals("/api/holders/1000000000000/tickets", Routes.holderTickets(id));
    }
}