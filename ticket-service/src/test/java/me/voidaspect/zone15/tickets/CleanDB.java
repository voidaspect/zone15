package me.voidaspect.zone15.tickets;

import org.junit.jupiter.api.Tag;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Tag(CleanDB.TAG_NAME)
public @interface CleanDB {

    String TAG_NAME = "clean-db";

}
