package me.voidaspect.zone15.tickets;

import com.fasterxml.jackson.databind.JsonNode;
import me.voidaspect.zone15.tickets.model.holder.Gender;
import me.voidaspect.zone15.tickets.model.holder.request.CreateHolderRequest;
import me.voidaspect.zone15.tickets.model.holder.response.HolderResponse;
import me.voidaspect.zone15.tickets.model.ticket.TicketStatus;
import me.voidaspect.zone15.tickets.model.ticket.request.CreateTicketRequest;
import me.voidaspect.zone15.tickets.model.ticket.response.TicketResponse;
import org.assertj.core.api.AbstractZonedDateTimeAssert;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

class TicketServiceApplicationTest extends TicketServiceRestTest {

    @Test
    void testGetAbsentHolder() {
        long holderId = 12345L;

        var response = rest.getForEntity(Routes.holder(holderId), JsonNode.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertIsJson(response);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    void testGetAbsentTicket() {
        long ticketId = 12345L;

        var response = rest.getForEntity(Routes.ticket(ticketId), JsonNode.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertIsJson(response);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    @CleanDB
    void testCreateHolder() {
        // 1 :: list page of holders, should be empty
        var allHoldersBeforeCreate = rest.getForEntity(Routes.HOLDERS, JsonNode.class);

        assertThat(allHoldersBeforeCreate.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertIsJson(allHoldersBeforeCreate);

        var allHoldersBeforeCreateBody = allHoldersBeforeCreate.getBody();

        assertThat(allHoldersBeforeCreateBody).isNotNull();
        assertThat(allHoldersBeforeCreateBody.get("content").isEmpty()).isTrue();

        // 2 :: create holder
        var createHolderRequest = testHolder();

        var holderCreated = rest.postForEntity(
                Routes.HOLDERS, createHolderRequest, JsonNode.class);

        assertThat(holderCreated.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertIsJson(holderCreated);

        var holderCreatedBody = holderCreated.getBody();

        assertThat(holderCreatedBody).isNotNull();
        assertHolderResponse(createHolderRequest, holderCreatedBody);
        var path = holderCreatedBody.get("path").asText();
        assertThat(holderCreated.getHeaders().getLocation()).hasToString(rest.getRootUri() + path);

        // 3 :: get created holder by id

        var getHolderById = rest.getForEntity(path, JsonNode.class);

        assertThat(getHolderById.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertIsJson(getHolderById);

        var getHolderByIdBody = getHolderById.getBody();

        assertThat(getHolderByIdBody).isNotNull();
        assertHolderResponse(createHolderRequest, getHolderByIdBody);

        // 4 :: list page of holders, should contain created holder
        var allHoldersAfterCreate = rest.getForEntity(Routes.HOLDERS, JsonNode.class);

        assertThat(allHoldersAfterCreate.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertIsJson(allHoldersAfterCreate);

        var allHoldersAfterCreateBody = allHoldersAfterCreate.getBody();

        assertThat(allHoldersAfterCreateBody).isNotNull();
        var page = allHoldersAfterCreateBody.get("content");
        assertThat(page.isArray()).isTrue();
        assertThat(page.isEmpty()).isFalse();
        assertThat(page.size()).isEqualTo(1);
        assertHolderResponse(createHolderRequest, page.get(0));
    }

    @Test
    @CleanDB
    void testCreateTicket() {
        // 1 create holder
        var holder = rest.postForObject(Routes.HOLDERS, testHolder(), HolderResponse.class);
        var holderTicketsPath = holder.tickets().path();

        // 2 create ticket
        var createTicketRequest1 = testTicket1();

        var holderId = holder.id();

        var ticketCreated1 = rest.postForEntity(holderTicketsPath, createTicketRequest1, JsonNode.class);

        assertThat(ticketCreated1.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertIsJson(ticketCreated1);

        var ticketCreatedBody1 = ticketCreated1.getBody();

        assertThat(ticketCreatedBody1).isNotNull();
        assertTicketResponse(holderId, createTicketRequest1, ticketCreatedBody1);
        assertThat(ticketCreatedBody1.get("status").asText()).isEqualTo(TicketStatus.REQUESTED.name());
        var ticket1Path = ticketCreatedBody1.get("path").asText();
        assertThat(ticketCreated1.getHeaders().getLocation()).hasToString(rest.getRootUri() + ticket1Path);

        // 3 create ticket without expiration time
        var createTicketRequest2 = testTicket2();

        var ticketCreated2 = rest.postForObject(holderTicketsPath, createTicketRequest2, JsonNode.class);

        assertThat(ticketCreated2).isNotNull();
        assertTicketResponse(holderId, createTicketRequest2, ticketCreated2);
        assertThat(ticketCreated2.get("status").asText()).isEqualTo(TicketStatus.REQUESTED.name());

        // 4 get ticket

        var getTicket = rest.getForEntity(ticket1Path, JsonNode.class);

        assertThat(getTicket.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertIsJson(getTicket);

        var getTicketBody = getTicket.getBody();
        assertThat(getTicketBody).isNotNull();

        assertTicketResponse(holderId, createTicketRequest1, getTicketBody);
        var history = getTicketBody.get("history");
        var usages = history.get("usages");
        var statusChanges = history.get("statusChanges");
        assertThat(usages.isArray()).isTrue();
        assertThat(usages.isEmpty()).isTrue();
        assertThat(statusChanges.isArray()).isTrue();
        assertThat(statusChanges.isEmpty()).isTrue();

        // 5 get tickets of a holder
        var holderTickets = rest.getForEntity(
                holderTicketsPath + "?size=2&sort=id", JsonNode.class);
        assertThat(holderTickets.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertIsJson(holderTickets);

        var holderTicketsBody = holderTickets.getBody();

        assertThat(holderTicketsBody).isNotNull();
        var page = holderTicketsBody.get("content");
        assertThat(page.isArray()).isTrue();
        assertThat(page.isEmpty()).isFalse();
        assertThat(page.size()).isEqualTo(2);
        assertTicketResponse(holderId, createTicketRequest1, page.get(0));
        assertTicketResponse(holderId, createTicketRequest2, page.get(1));
    }

    @Test
    @CleanDB
    void testDeleteHolder() {
        // 1 create holder
        var holder = rest.postForObject(Routes.HOLDERS, testHolder(), HolderResponse.class);
        // 3 create ticket
        var holderTicketsPath = holder.tickets().path();
        var createTicketRequest = testTicket2();
        var ticketCreated = rest.postForObject(
                holderTicketsPath, createTicketRequest, TicketResponse.class);
        // 4 delete holder
        var holderPath = holder.path();
        var holderDeleted = rest.exchange(
                holderPath, HttpMethod.DELETE, null, Void.class);

        assertThat(holderDeleted.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        // 5 check that holder is deleted
        var getHolder = rest.getForEntity(holderPath, JsonNode.class);

        assertThat(getHolder.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertIsJson(getHolder);

        // 6 check that ticket is deleted
        var getTicket = rest.getForEntity(ticketCreated.path(), JsonNode.class);

        assertThat(getTicket.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertIsJson(getTicket);

        // 7 check that repeated delete causes 404
        var holderDeletedSecondTime = rest.exchange(
                holderPath, HttpMethod.DELETE, null, JsonNode.class);

        assertThat(holderDeletedSecondTime.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertIsJson(holderDeletedSecondTime);
    }

    private void assertHolderResponse(CreateHolderRequest createHolderRequest, JsonNode responseBody) {
        long id = responseBody.get("id").asLong();
        assertThat(responseBody.get("path").asText()).isEqualTo(Routes.holder(id));
        assertThat(responseBody.get("tickets").get("path").asText()).isEqualTo(Routes.holderTickets(id));
        assertThat(responseBody.get("phoneNumber").asText()).isEqualTo(createHolderRequest.phoneNumber());
        var emailNode = responseBody.path("email");
        if (createHolderRequest.email() == null) {
            assertThat(emailNode.isMissingNode()).isTrue();
        } else {
            assertThat(emailNode.asText()).isEqualTo(createHolderRequest.email());
        }
        assertThat(responseBody.get("firstName").asText()).isEqualTo(createHolderRequest.firstName());
        var middleNameNode = responseBody.path("middleName");
        if (createHolderRequest.middleName() == null) {
            assertThat(middleNameNode.isMissingNode()).isTrue();
        } else {
            assertThat(middleNameNode.asText()).isEqualTo(createHolderRequest.middleName());
        }
        assertThat(responseBody.get("lastName").asText()).isEqualTo(createHolderRequest.lastName());
        assertThat(responseBody.get("dateOfBirth").asText())
                .isEqualTo(createHolderRequest.dateOfBirth().format(DateTimeFormatter.ISO_DATE));
        assertThat(responseBody.get("gender").asText()).isEqualTo(createHolderRequest.gender().name());
    }

    private void assertTicketResponse(long holderId,
                                      CreateTicketRequest createTicketRequest,
                                      JsonNode responseBody) {
        long id = responseBody.get("id").asLong();
        assertThat(responseBody.get("path").asText()).isEqualTo(Routes.ticket(id));

        assertThat(responseBody.get("maxUsages").asInt())
                .isEqualTo(Objects.requireNonNullElse(createTicketRequest.maxUsages(), 1).intValue());
        assertThat(responseBody.get("status").asText()).isNotBlank();
        assertIsIsoTimestampWithTZ(responseBody.get("createdAt").asText()).isBefore(ZonedDateTime.now());
        assertIsIsoTimestampWithTZ(responseBody.get("activatesAt").asText())
                .isEqualTo(normalizeTime(createTicketRequest.activatesAt()));
        var expiresAt = responseBody.path("expiresAt");
        if (createTicketRequest.expiresAt() != null) {
            assertIsIsoTimestampWithTZ(expiresAt.asText())
                    .isEqualTo(normalizeTime(createTicketRequest.expiresAt()));
        } else {
            assertThat(expiresAt.isMissingNode()).isTrue();
        }
        var holder = responseBody.get("holder");
        assertThat(holder.get("id").asLong()).isEqualTo(holderId);
        assertThat(holder.get("path").asText()).isEqualTo(Routes.holder(holderId));
    }

    private static void assertIsJson(ResponseEntity<?> holderCreatedResponse) {
        assertThat(holderCreatedResponse.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
    }

    private static ZonedDateTime normalizeTime(ZonedDateTime t) {
        return t.withZoneSameInstant(ZoneOffset.UTC).truncatedTo(ChronoUnit.MICROS);
    }

    private static AbstractZonedDateTimeAssert<?> assertIsIsoTimestampWithTZ(String text) {
        try {
            return assertThat(ZonedDateTime.parse(text, DateTimeFormatter.ISO_ZONED_DATE_TIME));
        } catch (DateTimeParseException e) {
            return fail(text + " is not a valid ISO zoned date-time value ", e);
        }
    }

    private static CreateHolderRequest testHolder() {
        return new CreateHolderRequest(
                "+1111111111",
                "test@email.com",
                "Tester",
                "Mock",
                "Testerson",
                LocalDate.of(1990, 1, 3),
                Gender.UNSPECIFIED
        );
    }

    private static CreateTicketRequest testTicket1() {
        var activatesAt = ZonedDateTime.now();
        return new CreateTicketRequest(
                (short) 2,
                activatesAt,
                activatesAt.plusDays(2)
        );
    }

    private static CreateTicketRequest testTicket2() {
        var activatesAt = ZonedDateTime.now();
        return new CreateTicketRequest(
                null,
                activatesAt,
                null
        );
    }

}
