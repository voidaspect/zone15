package me.voidaspect.zone15.tickets;

import me.voidaspect.zone15.test.postgres.Zone15PgContainer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.TestInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@IntegrationTest
public abstract class TicketServiceIntegrationTest {

    protected static final Logger log = LoggerFactory.getLogger(TicketServiceIntegrationTest.class);

    /**
     * Singleton shared database container
     */
    protected static final Zone15PgContainer DB;

    static {
        DB = new Zone15PgContainer().withDefaultOptions();
        DB.start();
    }

    @DynamicPropertySource
    static void datasourceProperties(DynamicPropertyRegistry registry) {
        DB.configure(registry);
    }

    @Autowired
    protected TestDBAutoCleaner testDBAutoCleaner;

    @AfterEach
    void tearDown(TestInfo testInfo) {
        if (testInfo.getTags().contains(CleanDB.TAG_NAME)) {
            log.info("Cleaning db after test {}", testInfo.getDisplayName());
            testDBAutoCleaner.cleanup();
        }
    }

}
