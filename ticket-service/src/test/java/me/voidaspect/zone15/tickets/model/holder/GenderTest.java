package me.voidaspect.zone15.tickets.model.holder;

import org.junit.jupiter.api.Test;

import static me.voidaspect.zone15.tickets.model.holder.Gender.*;

import static org.junit.jupiter.api.Assertions.*;

class GenderTest {

    @Test
    void testOrder() {
        assertEquals(0, MALE.ordinal());
        assertEquals(1, FEMALE.ordinal());
        assertEquals(2, UNSPECIFIED.ordinal());
    }

}