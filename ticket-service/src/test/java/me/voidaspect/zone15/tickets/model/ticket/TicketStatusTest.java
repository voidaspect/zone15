package me.voidaspect.zone15.tickets.model.ticket;

import org.junit.jupiter.api.Test;

import static me.voidaspect.zone15.tickets.model.ticket.TicketStatus.*;
import static org.junit.jupiter.api.Assertions.*;

class TicketStatusTest {

    @Test
    void testOrder() {
        assertEquals(0, REQUESTED.ordinal());
        assertEquals(1, CONFIRMED.ordinal());
        assertEquals(2, REJECTED.ordinal());
        assertEquals(3, CANCELLED.ordinal());
        assertEquals(4, REVOKED.ordinal());
        assertEquals(5, USED.ordinal());
    }

    @Test
    void testCanBeUsed() {
        assertFalse(REQUESTED.canBeUsed());
        assertTrue(CONFIRMED.canBeUsed());
        assertFalse(REJECTED.canBeUsed());
        assertFalse(CANCELLED.canBeUsed());
        assertFalse(REVOKED.canBeUsed());
        assertFalse(USED.canBeUsed());
    }

    @Test
    void testCanBeChangedTo() {
        assertFalse(REQUESTED.canChangeTo(REQUESTED));
        assertTrue(REQUESTED.canChangeTo(CONFIRMED));
        assertTrue(REQUESTED.canChangeTo(REJECTED));
        assertFalse(REQUESTED.canChangeTo(CANCELLED));
        assertFalse(REQUESTED.canChangeTo(REVOKED));
        assertFalse(REQUESTED.canChangeTo(USED));

        assertFalse(CONFIRMED.canChangeTo(REQUESTED));
        assertFalse(CONFIRMED.canChangeTo(CONFIRMED));
        assertFalse(CONFIRMED.canChangeTo(REJECTED));
        assertTrue(CONFIRMED.canChangeTo(CANCELLED));
        assertTrue(CONFIRMED.canChangeTo(REVOKED));
        assertFalse(CONFIRMED.canChangeTo(USED));

        assertFalse(REJECTED.canChangeTo(REQUESTED));
        assertFalse(REJECTED.canChangeTo(CONFIRMED));
        assertFalse(REJECTED.canChangeTo(REJECTED));
        assertFalse(REJECTED.canChangeTo(CANCELLED));
        assertFalse(REJECTED.canChangeTo(REVOKED));
        assertFalse(REJECTED.canChangeTo(USED));

        assertFalse(CANCELLED.canChangeTo(REQUESTED));
        assertFalse(CANCELLED.canChangeTo(CONFIRMED));
        assertFalse(CANCELLED.canChangeTo(REJECTED));
        assertFalse(CANCELLED.canChangeTo(CANCELLED));
        assertFalse(CANCELLED.canChangeTo(REVOKED));
        assertFalse(CANCELLED.canChangeTo(USED));

        assertFalse(REVOKED.canChangeTo(REQUESTED));
        assertFalse(REVOKED.canChangeTo(CONFIRMED));
        assertFalse(REVOKED.canChangeTo(REJECTED));
        assertFalse(REVOKED.canChangeTo(CANCELLED));
        assertFalse(REVOKED.canChangeTo(REVOKED));
        assertFalse(REVOKED.canChangeTo(USED));

        assertFalse(USED.canChangeTo(REQUESTED));
        assertFalse(USED.canChangeTo(CONFIRMED));
        assertFalse(USED.canChangeTo(REJECTED));
        assertFalse(USED.canChangeTo(CANCELLED));
        assertFalse(USED.canChangeTo(REVOKED));
        assertFalse(USED.canChangeTo(USED));
    }
}