package me.voidaspect.zone15.tickets.repository;

import me.voidaspect.zone15.tickets.CleanDB;
import me.voidaspect.zone15.tickets.TicketServicePersistenceTest;
import me.voidaspect.zone15.tickets.model.holder.Holder;
import me.voidaspect.zone15.tickets.model.holder.response.HolderResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@CleanDB
class HolderRepositoryTest extends TicketServicePersistenceTest {

    @Autowired
    private HolderRepository holderRepository;

    @Test
    void testListAsResponses() {
        var listHoldersBeforeCreate = holderRepository.listAsResponses(Pageable.unpaged());
        assertThat(listHoldersBeforeCreate.getTotalElements()).isEqualTo(0);

        var testHolders = persistDefaultTestHolders();

        var listHoldersAfterCreate = holderRepository
                .listAsResponses(PageRequest.of(0, 5, Sort.by("phoneNumber")));

        assertThat(listHoldersAfterCreate.getTotalPages()).isEqualTo(1);
        assertThat(listHoldersAfterCreate.getNumberOfElements()).isEqualTo(3);
        assertThat(listHoldersAfterCreate.getTotalElements()).isEqualTo(3);

        var responsesAfterCreate = listHoldersAfterCreate.getContent();

        assertResponseMatchesHolder(testHolders.get(0), responsesAfterCreate.get(0));
        assertResponseMatchesHolder(testHolders.get(1), responsesAfterCreate.get(1));
        assertResponseMatchesHolder(testHolders.get(2), responsesAfterCreate.get(2));

        var listFirst2HoldersAfterCreate = holderRepository
                .listAsResponses(PageRequest.of(0, 2, Sort.by("email")));

        assertThat(listFirst2HoldersAfterCreate.getTotalPages()).isEqualTo(2);
        assertThat(listFirst2HoldersAfterCreate.getNumberOfElements()).isEqualTo(2);
        assertThat(listFirst2HoldersAfterCreate.getTotalElements()).isEqualTo(3);

        var first2ResponsesAfterCreate = listHoldersAfterCreate.getContent();

        assertResponseMatchesHolder(testHolders.get(0), first2ResponsesAfterCreate.get(0));
        assertResponseMatchesHolder(testHolders.get(1), first2ResponsesAfterCreate.get(1));
    }

    @Test
    void testFindByIdAsResponse() {
        var unknownId = 1L;
        var unknownHolderResponse = holderRepository.findByIdAsResponse(unknownId);
        assertThat(unknownHolderResponse).isEmpty();

        var holder = testHolder1();
        entityManager.persist(holder);

        var knownHolderResponse = holderRepository.findByIdAsResponse(holder.getId());
        assertThat(knownHolderResponse).hasValueSatisfying(response -> assertResponseMatchesHolder(holder, response));
    }

    private List<Holder> persistDefaultTestHolders() {
        var holder1 = testHolder1();
        var holder2 = testHolder2();
        var holder3 = testHolder3();

        entityManager.persist(holder1);
        entityManager.persist(holder2);
        entityManager.persist(holder3);

        return List.of(holder1, holder2, holder3);
    }

    private static void assertResponseMatchesHolder(Holder holder, HolderResponse response) {
        assertThat(response.id()).isEqualTo(holder.getId());
        assertThat(response.phoneNumber()).isEqualTo(holder.getPhoneNumber());
        assertThat(response.email()).isEqualTo(holder.getEmail());
        assertThat(response.firstName()).isEqualTo(holder.getFirstName());
        assertThat(response.middleName()).isEqualTo(holder.getMiddleName());
        assertThat(response.lastName()).isEqualTo(holder.getLastName());
        assertThat(response.dateOfBirth()).isEqualTo(holder.getDateOfBirth());
        assertThat(response.gender()).isEqualTo(holder.getGender());
    }

}