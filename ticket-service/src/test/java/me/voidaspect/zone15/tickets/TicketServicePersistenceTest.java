package me.voidaspect.zone15.tickets;

import me.voidaspect.zone15.tickets.model.holder.Gender;
import me.voidaspect.zone15.tickets.model.holder.Holder;
import me.voidaspect.zone15.tickets.model.ticket.Ticket;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;

@DataJpaTest(showSql = false, includeFilters = @ComponentScan.Filter(
        type = FilterType.ASSIGNABLE_TYPE, classes = TestDBAutoCleaner.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public abstract class TicketServicePersistenceTest extends TicketServiceIntegrationTest {

    @PersistenceContext
    protected EntityManager entityManager;

    protected static Holder testHolder1() {
        var holder1 = new Holder();
        holder1.setPhoneNumber("+1111111111");
        holder1.setEmail("test1@email.com");
        holder1.setFirstName("holder-one-f-name");
        holder1.setMiddleName("holder-one-m-name");
        holder1.setLastName("holder-one-l-name");
        holder1.setDateOfBirth(LocalDate.of(1990, 1, 3));
        holder1.setGender(Gender.MALE);
        return holder1;
    }

    protected static Holder testHolder2() {
        var holder2 = new Holder();
        holder2.setPhoneNumber("+1111111112");
        holder2.setEmail("test2@email.com");
        holder2.setFirstName("holder-two-f-name");
        holder2.setMiddleName("holder-two-m-name");
        holder2.setLastName("holder-two-l-name");
        holder2.setDateOfBirth(LocalDate.of(1995, 4, 13));
        holder2.setGender(Gender.FEMALE);
        return holder2;
    }

    // lacks optional fields (email, middle name)
    protected static Holder testHolder3() {
        var holder3 = new Holder();
        holder3.setPhoneNumber("+1111111113");
        holder3.setFirstName("holder-three-f-name");
        holder3.setLastName("holder-last-l-name");
        holder3.setDateOfBirth(LocalDate.of(2001, 9, 1));
        holder3.setGender(Gender.UNSPECIFIED);
        return holder3;
    }

    protected static Ticket testTicket(Holder holder) {
        var ticket = new Ticket();
        ticket.setHolder(holder);
        return ticket;
    }


}
