package me.voidaspect.zone15.tickets;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
@Profile(TestProfiles.INTEGRATION_TEST)
public class TestDBAutoCleaner {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void cleanup() {
        entityManager.createNativeQuery("truncate holders cascade").executeUpdate();
        entityManager.clear();
    }

}
