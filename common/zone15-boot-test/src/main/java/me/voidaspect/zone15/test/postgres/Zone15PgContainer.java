package me.voidaspect.zone15.test.postgres;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.WaitStrategy;

import java.time.Duration;
import java.util.Map;

public class Zone15PgContainer extends PostgreSQLContainer<Zone15PgContainer> {

    private static final Logger log = LoggerFactory.getLogger(Zone15PgContainer.class);

    private static final String DOCKER_IMAGE = "postgres:13";

    private static final Duration DEFAULT_STARTUP_TIMEOUT = Duration.ofSeconds(5);

    private static final WaitStrategy DEFAULT_WAIT_STRATEGY = new PostgreSQLWaitStrategy();

    public Zone15PgContainer() {
        super(DOCKER_IMAGE);
    }

    public Zone15PgContainer withDefaultOptions() {
        return withTmpFs(Map.of("/var/lib/postgresql/data", "rw"))
                .withStartupTimeout(DEFAULT_STARTUP_TIMEOUT)
                .waitingFor(DEFAULT_WAIT_STRATEGY);
    }

    public void configure(DynamicPropertyRegistry registry) {
        log.info("Configuring spring datasource to use test container");

        registry.add("spring.datasource.url", this::getJdbcUrl);
        registry.add("spring.datasource.username", this::getUsername);
        registry.add("spring.datasource.password", this::getPassword);
    }

}
