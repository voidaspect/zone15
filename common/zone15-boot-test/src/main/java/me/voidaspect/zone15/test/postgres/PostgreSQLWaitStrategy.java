package me.voidaspect.zone15.test.postgres;

import org.testcontainers.DockerClientFactory;
import org.testcontainers.containers.ContainerLaunchException;
import org.testcontainers.containers.output.OutputFrame;
import org.testcontainers.containers.output.WaitingConsumer;
import org.testcontainers.containers.wait.strategy.AbstractWaitStrategy;
import org.testcontainers.utility.LogUtils;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * Based on {@link org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy}
 * Improves efficiency by pre-compiling the regex pattern
 */
public class PostgreSQLWaitStrategy extends AbstractWaitStrategy {

    private static final Pattern CONTAINER_AVAILABLE_LOG_MESSAGE_PATTERN = Pattern
            .compile(".*database system is ready to accept connections\\n$");

    private static final int TIMES = 1;

    @Override
    protected void waitUntilReady() {
        var waitingConsumer = new WaitingConsumer();
        var dockerClient = DockerClientFactory.instance().client();

        LogUtils.followOutput(dockerClient, waitStrategyTarget.getContainerId(), waitingConsumer);

        Predicate<OutputFrame> waitPredicate = outputFrame ->
                CONTAINER_AVAILABLE_LOG_MESSAGE_PATTERN.matcher(outputFrame.getUtf8String()).matches();

        try {
            waitingConsumer.waitUntil(waitPredicate, startupTimeout.getSeconds(), TimeUnit.SECONDS, TIMES);
        } catch (TimeoutException e) {
            throw new ContainerLaunchException("Timed out waiting for log output matching '" +
                    CONTAINER_AVAILABLE_LOG_MESSAGE_PATTERN + "'");
        }
    }

}
