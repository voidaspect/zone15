package me.voidaspect.zone15.venue.repository;

import me.voidaspect.zone15.venue.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface EventRepository extends JpaRepository<Event, Long> {

    @Query("select e from Event e join fetch e.venue where e.id = ?1")
    Optional<Event> findByIdFetchVenue(long id);

}
