package me.voidaspect.zone15.venue.model;

import me.voidaspect.zone15.venue.rpc.CreateVenueRequest;

public class CreateVenueWithResourceRequest {

    private final CreateVenueRequest innerRequest;

    private final long resourceId;

    public CreateVenueWithResourceRequest(CreateVenueRequest innerRequest, long resourceId) {
        this.innerRequest = innerRequest;
        this.resourceId = resourceId;
    }

    public CreateVenueRequest getInnerRequest() {
        return innerRequest;
    }

    public long getResourceId() {
        return resourceId;
    }
}
