package me.voidaspect.zone15.venue.rpc;

import com.google.protobuf.Empty;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import me.voidaspect.zone15.availability.rpc.*;
import me.voidaspect.zone15.venue.model.CreateVenueWithResourceRequest;
import me.voidaspect.zone15.venue.service.EventOperations;
import me.voidaspect.zone15.venue.service.VenueOperations;
import net.devh.boot.grpc.client.inject.GrpcClient;
import net.devh.boot.grpc.server.service.GrpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@GrpcService
public class VenueGrpcService extends VenueGrpc.VenueImplBase {

    private static final Logger log = LoggerFactory.getLogger(VenueGrpcService.class);

    private AvailabilityGrpc.AvailabilityBlockingStub availability;

    private final EventOperations events;

    private final VenueOperations venues;

    public VenueGrpcService(EventOperations events, VenueOperations venues) {
        this.events = events;
        this.venues = venues;
    }

    @GrpcClient("availability")
    public VenueGrpcService setAvailabilityClient(AvailabilityGrpc.AvailabilityBlockingStub availability) {
        this.availability = availability;
        return this;
    }

    @Override
    public void createVenue(CreateVenueRequest request, StreamObserver<VenueId> responseObserver) {
        try {
            long resourceId = availability.createResource(CreateResourceRequest.newBuilder()
                    .setName(request.getName())
                    .setDescription(request.getDescription())
                    .setCapacity(request.getCapacity())
                    .build()).getId();
            long venueId = venues.create(new CreateVenueWithResourceRequest(request, resourceId));
            responseObserver.onNext(VenueId.newBuilder().setId(venueId).build());
            responseObserver.onCompleted();
        } catch (Exception e) {
            responseObserver.onError(Status.INTERNAL.withCause(e).asException());
        }
    }

    @Override
    public void getVenue(VenueId request, StreamObserver<VenueResponse> responseObserver) {
        try {
            var info = venues.get(request.getId()).orElseThrow(() -> new StatusException(Status.NOT_FOUND));
            var resource = availability.getResource(ResourceId.newBuilder().setId(info.getResourceId()).build());
            var response = VenueResponse.newBuilder()
                    .setContactEmail(info.getContactEmail())
                    .setName(info.getName())
                    .setDescription(info.getDescription())
                    .setCapacity(resource.getCapacity())
                    .build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        } catch (Exception e) {
            responseObserver.onError(e);
        }
    }

    @Override
    public void deleteVenue(VenueId request, StreamObserver<Empty> responseObserver) {
        try {
            long resourceId = venues.get(request.getId())
                    .orElseThrow(() -> new StatusException(Status.NOT_FOUND))
                    .getResourceId();
            var empty = availability.deleteResource(ResourceId.newBuilder().setId(resourceId).build());
            venues.delete(resourceId);
            responseObserver.onNext(empty);
            responseObserver.onCompleted();
        } catch (Exception e) {
            responseObserver.onError(e);
        }
    }

    @Override
    public void tryBeginEvent(VenueId request, StreamObserver<TryBeginEventResponse> responseObserver) {
        var responseBuilder = TryBeginEventResponse.newBuilder();
        try {
            var info = venues.get(request.getId()).orElseThrow(() -> new StatusException(Status.NOT_FOUND));
            var acquired = availability.acquireResource(AcquireResourceRequest.newBuilder()
                    .setId(info.getResourceId())
                    .setParties(1)
                    .build());
            log.info("{} acquired, total parties: {}", info.getName(), acquired.getParties());
            var event = events.start(request.getId());
            responseBuilder.setSuccessful(event);
        } catch (Exception e) {
            responseBuilder.setFailed(EventNotStartedResponse.newBuilder().setReason(e.getMessage()).build());
        }
        responseObserver.onNext(responseBuilder.build());
        responseObserver.onCompleted();
    }

    @Override
    public void tryFinishEvent(EventId request, StreamObserver<EventEndedResponse> responseObserver) {
        try {
            var event = events.get(request.getId());
            EventEndedResponse response;
            if (event.getEndedAt() == null) {
                long resourceId = event.getVenue().getResourceId();
                var released = availability.releaseResource(ReleaseResourceRequest.newBuilder()
                        .setId(resourceId)
                        .setParties(1)
                        .build());
                log.info("{} released, total parties: {}", event.getVenue().getName(), released.getParties());
                response = events.end(request.getId());
            } else {
                response = EventEndedResponse.newBuilder()
                        .setEndedAt(event.getEndedAt().toEpochMilli())
                        .build();
            }
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        } catch (Exception e) {
            responseObserver.onError(e);
        }
    }
}
