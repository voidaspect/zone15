package me.voidaspect.zone15.venue.service;

import io.grpc.Status;
import io.grpc.StatusException;
import me.voidaspect.zone15.venue.model.CreateVenueWithResourceRequest;
import me.voidaspect.zone15.venue.model.Venue;
import me.voidaspect.zone15.venue.model.VenueInfo;
import me.voidaspect.zone15.venue.repository.VenueRepository;
import me.voidaspect.zone15.venue.rpc.CreateVenueRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class VenueService implements VenueOperations {

    private final VenueRepository repository;

    public VenueService(VenueRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public long create(CreateVenueWithResourceRequest request) {
        var data = request.getInnerRequest();
        var venue = new Venue();
        venue.setName(data.getName());
        venue.setDescription(data.getDescription());
        venue.setResourceId(request.getResourceId());
        venue.setContactEmail(data.getContactEmail());
        repository.save(venue);
        return venue.getId();
    }

    @Override
    @Transactional
    public void delete(long id) throws StatusException {
        var venue = repository.findById(id)
                .orElseThrow(() -> new StatusException(Status.NOT_FOUND));
        repository.delete(venue);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<VenueInfo> get(long id) {
        return repository.findById(id).map(venue -> new VenueInfo(
                venue.getResourceId(), venue.getName(), venue.getDescription(), venue.getContactEmail()));
    }
}
