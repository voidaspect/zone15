package me.voidaspect.zone15.venue.service;

import io.grpc.Status;
import io.grpc.StatusException;
import me.voidaspect.zone15.venue.model.Event;
import me.voidaspect.zone15.venue.repository.EventRepository;
import me.voidaspect.zone15.venue.repository.VenueRepository;
import me.voidaspect.zone15.venue.rpc.EventEndedResponse;
import me.voidaspect.zone15.venue.rpc.EventStartedResponse;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Service
@Transactional
public class EventService implements EventOperations {

    private final EventRepository eventRepository;

    private final VenueRepository venueRepository;

    public EventService(EventRepository eventRepository, VenueRepository venueRepository) {
        this.eventRepository = eventRepository;
        this.venueRepository = venueRepository;
    }

    @Override
    public EventStartedResponse start(long venueId) throws StatusException {
        if (!venueRepository.existsById(venueId)) {
            throw new StatusException(Status.NOT_FOUND.withDescription("Venue not found: " + venueId));
        }
        var event = new Event();
        event.setVenue(venueRepository.getOne(venueId));
        event.setStartedAt(Instant.now());
        eventRepository.save(event);

        return EventStartedResponse.newBuilder()
                .setEventId(event.getId())
                .setStartedAt(event.getStartedAt().toEpochMilli())
                .build();
    }

    @Override
    public EventEndedResponse end(long eventId) throws StatusException {
        var event = eventRepository.findById(eventId)
                .orElseThrow(() -> new StatusException(Status.NOT_FOUND));

        var endedAt = Instant.now();
        event.setEndedAt(endedAt);

        return EventEndedResponse.newBuilder()
                .setEndedAt(endedAt.toEpochMilli())
                .build();
    }

    @Override
    public Event get(long id) throws StatusException {
        return eventRepository.findByIdFetchVenue(id)
                .orElseThrow(() -> new StatusException(Status.NOT_FOUND));
    }
}
