package me.voidaspect.zone15.venue.model;

public class VenueInfo {

    private final long resourceId;

    private final String name;

    private final String description;

    private final String contactEmail;

    public VenueInfo(long resourceId, String name, String description, String contactEmail) {
        this.resourceId = resourceId;
        this.name = name;
        this.description = description;
        this.contactEmail = contactEmail;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public long getResourceId() {
        return resourceId;
    }
}
