package me.voidaspect.zone15.venue.service;

import io.grpc.StatusException;
import me.voidaspect.zone15.venue.model.Event;
import me.voidaspect.zone15.venue.rpc.EventEndedResponse;
import me.voidaspect.zone15.venue.rpc.EventStartedResponse;

public interface EventOperations {

    EventStartedResponse start(long venueId) throws StatusException;

    EventEndedResponse end(long eventId) throws StatusException;

    Event get(long id) throws StatusException;

}
