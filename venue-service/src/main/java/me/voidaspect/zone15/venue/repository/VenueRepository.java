package me.voidaspect.zone15.venue.repository;

import me.voidaspect.zone15.venue.model.Venue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VenueRepository extends JpaRepository<Venue, Long> {
}
