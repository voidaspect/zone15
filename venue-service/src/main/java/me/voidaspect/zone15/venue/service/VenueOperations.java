package me.voidaspect.zone15.venue.service;

import io.grpc.StatusException;
import me.voidaspect.zone15.venue.model.CreateVenueWithResourceRequest;
import me.voidaspect.zone15.venue.model.VenueInfo;

import java.util.Optional;

public interface VenueOperations {

    long create(CreateVenueWithResourceRequest request);

    void delete(long id) throws StatusException;

    Optional<VenueInfo> get(long id);

}
