create sequence venues_id_seq increment 10;

create table venues
(
    id            bigint not null default nextval('venues_id_seq'),
    resource_id   bigint not null,
    name          text   not null,
    description   text,
    contact_email text,

    primary key (id)
);

create sequence events_id_seq increment 10;

create table events
(
    id         bigint    not null default nextval('events_id_seq'),
    venue_id   bigint    not null references venues (id) on delete cascade,
    started_at timestamp not null,
    ended_at   timestamp,

    primary key (id)
);

create index on events (venue_id);